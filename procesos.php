<?php
require_once __DIR__ . '/logic/utl.php';
$cb = utilities::load_template();
?>
    <div class="content">

        <div class="row">
            <div class="col-12">
                <div class="block block-themed">
                    <div class="block-header">
                        <h3 class="block-title">Procesos</h3>
                    </div>
                    <div class="block-content">
                        <div class="row form-group">
                            <div class="col-12 text-center">
                                <form opc="getProcesos" target="#tblProcesos">
                                    <button type="submit" class="btn btn-primary">Buscar <i class="fa fa-search"></i>
                                    </button>
                                </form>
                                <button type="button" class="btn btn-success" id="agregarProceso">Agregar Proceso <i class="fa fa-plus"></i></button>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-6" id="tblProcesos">

                            </div>
                            <div class="col-6" id="tblEmpleadosAprueban">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="modal fade" id="modalProceso" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popin modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <form opc="saveProceso" target="#msgProceso">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Proceso <!-- TITULO --></h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>

                <div class="block-content">

                        <div class="row">
                            <div class="col-12 form-group">
                                <label>Nombre: </label>
                                <input type="text" name="nombre" class="form-control" placeholder="Ingresar nombre del proceso">
                            </div>
                            <div class="col-12 form-group">
                                <label>Descripcion: </label>
                                <input type="text" name="descripcion" class="form-control" placeholder="Ingresar descripcion">
                            </div>
                            <div class="col-12 form-group">
                                <label>Fecha Inicio: </label>
                                <input type="text" name="fecha_inicio" class="form-control datepicker">
                            </div>
                            <div class="col-12 form-group">
                                <label>Fecha Fin: </label>
                                <input type="text" name="fecha_fin" class="form-control datepicker">
                            </div>
                            <div class="col-12 form-group" id="msgProceso"></div>
                        </div>


                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                <button type="submit" class="btn btn-alt-primary" id="guardar_proceso">
                    <i class="fa fa-check"></i> Guardar Proceso
                </button>
            </div>

        </div>
        </form>
    </div>
</div>
<script>

    $('form').submit(function(e){
        e.preventDefault();
        send_request('logic/procesos_logic.php',$(this)[0]);
    });

    $('#agregarProceso').click(function(){
        $('#modalProceso form').attr('data-id',0);
        $('#modalProceso').modal('show');
    });
</script>

<?php utilities::load_template_footer($cb); ?>