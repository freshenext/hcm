<?php
require_once __DIR__ . '/logic/utl.php';
$cb = utilities::load_template();
?>

<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="block block-themed">
                <div class="block-header">
                    <h3 class="block-title">Roles</h3>
                </div>
                <div class="block-content">
                    <div class="row">
                        <div class="col-12">

                        </div>
                        <div class="col-12 text-center">
                            <button type="button" class="btn btn-primary" id="buscar_rol">Buscar <i class="fa fa-search"></i></button>
                            <button type="button" class="btn btn-success" id="agregar_rol">Agregar Rol <i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12" id="tbl_roles">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_roles" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popin">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Roles<!-- TITULO --></h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <span hidden id="rol_id_edit"></span>
                    <div class="row">
                        <!-- CONTENIDO -->
                        <div class="col-12 form-group">
                            <label>Descripción: </label>
                            <input type="text" class="form-control" id="rol_desc">
                        </div>
                        <div  class="col-12 form-group">
                            <label>Es Recursos Humanos: <input type="checkbox" id="rol_es_rrhh"></label>
                        </div>
                        <div  class="col-12 form-group">
                            <label>Activo: <input type="checkbox" id="rol_activo"></label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12" id="msgErrorRol"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-alt-primary" id="guardar_rol">
                    <i class="fa fa-check"></i> Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_rol_opciones" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popin">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Opciones del Rol<!-- TITULO --></h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <span hidden id="txt_rol_id"></span>
                    <div class="row form-group">
                        <!-- FILTROS -->
                        <div class="col-12 form-group">
                            <select class="form-control" id="sel_opciones"></select>
                        </div>
                        <div class="col-12 text-center">
                            <button type="button" class="btn btn-primary" id="agregar_rol_opciones">Agregar opcion<i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <hr>
                    <div class="row form-group">
                        <div class="col-12" id="msgRolOpciones"></div>
                    </div>
                    <div class="row form-group">

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-alt-primary">
                    <i class="fa fa-check"></i> Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    function buscar_roles(){
        const obj = {
            opc : 'get_roles'
        }
        send_request('logic/roles_logic.php', obj, '#tbl_roles');
    }

    function buscar_opciones_rol(){
        const rol_id = $('#txt_rol_id').text();
        const obj = {
            opc : 'get_opc_rol',
            rol_id : rol_id
        }
        send_request('logic/roles_logic.php', obj, '#msgRolOpciones');
    }

    function populate_opciones_sel(){
        var comp = $('#sel_opciones');
        const obj = {
            opc : 'get_sel_opciones'
        }
        send_request('logic/roles_logic.php', obj, comp);
    }

    function agregar_opcion_a_rol(){
        const opcion_id = parseInt($('#sel_opciones option:selected').val()) || 0;
        const rol_id = $('#txt_rol_id').text();

        if(opcion_id == 0){
            return;
        }
        const obj = {
            opc : 'agregar_rol_opciones',
            opcion_id : opcion_id,
            rol_id : rol_id
        }

        send_request('logic/roles_logic.php', obj, '#msgRolOpciones', function(){
            buscar_opciones_rol();
        });
    }

    $('body').on('click','#borrar_opc_rol', function(){
        const id = parseInt($(this).closest('tr').attr('data-id')) || 0;

        if(id == 0){
            return;
        }
        const obj = {
            opc : 'borrar_rol_opc',
            id : id
        }
        send_request('logic/roles_logic.php', obj, '#msgRolOpciones', function(){
            buscar_opciones_rol();
        });
    });

    $('#agregar_rol_opciones').click(function(){
        agregar_opcion_a_rol();
    });

    $(document).ready(function(){
        buscar_roles();
        populate_opciones_sel();
    });

    $('#buscar_rol').click(function(){
        buscar_roles();
    });

    $('body').on('click','[name=btn_ver_opciones]', function(){
        $('#txt_rol_id').text($(this).closest('tr').attr('data-id'));
        buscar_opciones_rol();
        $('#modal_rol_opciones').modal();
    });

    $('body').on('click', '#edit_rol', function(){
        limpiar_modal_rol(); /* Limpiar rol */
        /* Asignar valores */
        $('#rol_desc').val($(this).closest('tr').find('#td_rol_desc').text());
        $('#rol_activo').prop('checked', parseInt($(this).closest('tr').find('#td_rol_activo').attr('data-activo')));
        $('#rol_id_edit').text($(this).closest('tr').attr('data-id'));
        $('#rol_es_rrhh').prop('checked', parseInt($(this).closest('tr').attr('data-es-rrhh')));
        /* Abrir modal */
        $('#modal_roles').modal('show');
    })

    function limpiar_modal_rol(){
        $('#rol_desc').val('');
        $('#rol_activo').prop('checked', true);
        $('#rol_es_rrhh').prop('checked',false);
        $('#rol_id_edit').text('');
    }

    $('#agregar_rol').click(function(){
        /* Nuevo rol */
        limpiar_modal_rol();
        $('#modal_roles').modal();
    });

    $('#guardar_rol').click(function(){
        guardar_rol();
    });

    function guardar_rol(){
        const desc = $('#rol_desc').val();
        const activo = $('#rol_activo').prop('checked') ? 1 : 0;
        const id = $('#rol_id_edit').text();
        const es_rrhh = $('#rol_es_rrhh').prop('checked') ? 1 : 0;
        var comp_error = $('#msgErrorRol');

        if(desc == ''){
            comp_error(send_danger_msg('Error: Debe de introducir una descripción.'));
            return;
        }

        const obj = {
            opc : 'save_rol',
            desc : desc,
            activo : activo,
            id : id,
            es_rrhh : es_rrhh
        }
        send_request('logic/roles_logic.php', obj, comp_error);
    }
</script>


<?php
utilities::load_template_footer($cb);
?>
