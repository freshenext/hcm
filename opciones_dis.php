<?php
require_once __DIR__ . '/logic/utl.php';

$cb = utilities::load_template();

?>

<!-- MODALS -->
<?php include_once __DIR__ . '/modal/modal_opcion.php';?>
<?php include_once __DIR__ . '/modal/modal_categorias.php'; ?>
<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="block block-themed">
                <div class="block-header">
                    <h3 class="block-title">Opciones</h3>
                </div>
                <div class="block-content">
                    <!-- Filtros de busqueda -->
                    <div class="row">
                        <div class="col-12 text-center">
                            <button type="button" class="btn btn-primary" id="opcion_add">Añadir opción</button>
                        </div>
                    </div>
                    <hr>
                    <!-- Tabla -->
                    <div class="row">
                        <div class="col-12" id="tbl_opciones">

                        </div>
                    </div>
                    <script>

                        $('#opcion_add').click(function () {
                            nueva_opcion();
                        });

                        $('body').on('click', '[name=opc_editar]', function(){
                            var tr = $(this).closest('tr');
                            var desc = tr.find('.descripcion').text();
                            var link = tr.find('.link').text();
                            var activada = tr.find('.activada').attr('data-bool');
                            var id = $(this).closest('tr').attr('data-id');
                            edit_opcion(id, desc, link, activada);
                        });

                        $('#btn_opcion_guardar').click(function () {
                            guardar_opcion(function () {
                                load_opciones();
                                $('#modal_opciones').modal('hide');
                            });
                        });

                        function load_opciones(){
                            var obj = {
                                opc : 'get_opciones'
                            }
                            send_request('logic/opciones_logic.php', obj, '#tbl_opciones', '');
                        }

                        load_opciones();
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>



<?php
utilities::load_template_footer($cb);
?>
