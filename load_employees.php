<?php require_once __DIR__ . '/logic/utl.php';
$cb = utilities::load_template();
?>
    <!-- Page Content -->
    <div class="content">
        <div class="row">
            <div class="col-12">
                <div class="block block-themed">
                    <div class="block-header">
                        <h3 class="block-title">Cargar empleados</h3>
                    </div>
                    <div class="block-content">
                        <div class="row form-group">
                            <div class="col-12">
                                <div class="text-center"><button id="cargar_empleados" type="button" class="btn btn-primary">
                                        Cargar Empleados
                                    </button></div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12">
                                <span></span>
                            </div>
                            <div class="col-12" id="tbl_empleados">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->


    <script>
        function get_empleados_if_exist(){
            var obj = {
                opc : 'get_empleados'
            }
            send_request('logic/empleados_logic.php', obj, '#tbl_empleados');
        }

        function get_sap_empleados(){
            var obj = {
                opc : 'get_sap_empleados'
            }

            send_request('logic/empleados_logic.php', obj, '#tbl_empleados');
        }


        $(document).ready(function () {

            $('#cargar_empleados').click(function () {
                get_sap_empleados();
            });

        });
    </script>
    <!-- Page JS Plugins -->
<?php utilities::load_template_footer($cb); ?>