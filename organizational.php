<?php

require_once __DIR__ . '/logic/utl.php';


$cb = utilities::load_template();


?>
<style>
    #div_organigrama table {
        border-collapse: unset !important;
    }
    #div_organigrama {
        overflow: scroll !important;
    }
</style>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>

<div class="content">
    <div class="block block-themed">
        <div class="block-header">
            <h3 class="block-title">Organigrama</h3>
        </div>
        <div class="block-content">
            <!-- Filtros -->
            <div class="row form-group">
                <div class="col-6 form-group">
                    <label>Tipo de Organigrama: </label>
                    <select class="form-control" id="sel_via_evaluaciones"></select>
                </div>
                <div class="col-6 form-group">
                    <label>Fecha: </label>
                    <input type="text" class="js-flatpickr form-control" id="fecha"
                           placeholder="Click para seleccionar fecha">
                </div>
                <!-- BTN Buscar -->
                <div class="col-12">
                    <button type="button" class="btn btn-primary" id="get_organigrama"><b>Buscar</b> <i
                                class="fa fa-search"></i></button>
                    <button type="button" class="btn btn-info" id="imprimir">Imprimir <i class="fa fa-print"></i></button>
                </div>
            </div>
            <!-- ERROR -->
            <div class="row form-group">
                <div class="col-12 text-center" id="msgOrg"></div>
            </div>
            <!-- ORG -->
            <div class="row form-group">
                <div class="col-12" id="div_organigrama"></div>
            </div>
        </div>
    </div>
</div>

<script>
    function buscar_sel_vias_eval() {

        const obj = {
            opc: 'get_sel_vias_evaluacion'
        }
        send_request('logic/organigrama_logic.php', obj, '#sel_via_evaluaciones');
    }

    function get_organigrama() {
        const via_eval = $('#sel_via_evaluaciones option:selected').val();
        const fecha = $('#fecha').val();
        const comp_error = $('#msgOrg').html('');
        if (fecha == '') {
            comp_error.html(send_danger_msg('Error: Debe introducir una fecha'));
            return;
        }
        if (via_eval == '0') {
            comp_error.html(send_danger_msg('Error: Debe seleccionar un tipo de organigrama.'));
            return;
        }
        const obj = {
            opc: 'get_organigrama',
            via_evaluacion: via_eval,
            fecha: fecha
        }

        send_request('logic/organigrama_logic.php', obj, '#div_organigrama');
    }


    $('#get_organigrama').click(function () {
        get_organigrama();
    });

    $('#imprimir').click(function(){
        if($('#div_organigrama table').length > 0)
            $('#div_organigrama').printThis();
    });
    $(document).ready(function () {
        buscar_sel_vias_eval();

        $('.js-flatpickr').flatpickr({
            altFormat : 'F j, Y',
            altInput : true,
            defaultDate : new Date()
        });
    });

    google.charts.load('current', {packages: ["orgchart"]});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Name');
        data.addColumn('string', 'Manager');
        data.addColumn('string', 'ToolTip');

        // For each orgchart box, provide the name, manager, and tooltip to show.

        // Create the chart.
        var chart = new google.visualization.OrgChart(document.getElementById('div_organigrama'));
        // Draw the chart, setting the allowHtml option to true for the tooltips.

        chart.draw(data, {'allowHtml': true});


    }
</script>

<?php utilities::load_template_footer($cb); ?>
