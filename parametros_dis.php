<?php
require_once __DIR__ . '/logic/utl.php';

$cb = utilities::load_template();
?>

<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="block block-themed">
                <div class="block-header">
                    <h3 class="block-title">Parámetros</h3>
                </div>
                <div class="block-content">

                    <form id="formParametros">
                        <input type="text" hidden value="save_parametros" name="opc"/>
                        <input type="text" hidden value="0" name="id" />
                        <div class="row form-group">
                            <div class="col-12">
                                <h4 class="form-group">Parámetro de Imágenes de Organigrama</h4>
                            </div>
                            <div class="col-4">
                                <label>Imágen para Empleado sin foto: </label>
                                <input type="file" name="imgEmpleado">
                            </div>
                            <div class="col-4">
                                <label>Imágen para Puesto sin foto: </label>
                                <input type="file" name="imgPuesto">
                            </div>
                            <div class="col-4">
                                <label>Imágen para Organización sin foto: </label>
                                <input type="file" name="imgEmpresa">
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12">
                                <button type="submit" class="btn btn-success">Guardar Parámetros</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('#formParametros').submit(function(e){
        e.preventDefault();
        send_request('logic/parametros_logic.php', this, '');
    });
</script>
<?php

utilities::load_template_footer($cb);
?>