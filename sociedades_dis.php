<?php

require_once __DIR__ . '/logic/utl.php';

$cb = utilities::load_template();

?>

<div class="content">
    <div class="block block-themed">
        <div class="block-header">
            <h3 class="block-title">Sociedades</h3>
        </div>
        <div class="block-content">

            <div  class="row form-group">
                <div class="col-12 text-center">
                    <button type="button" class="btn btn-success" id="btn_sociedades">Obtener Sociedades <i class="fa fa-check-circle-o"></i></button>
                </div>
            </div>
            <div  class="row">
                <div class="col-12" id="tbl_sociedades"></div>
            </div>

        </div>
    </div>
</div>
<div class="modal fade" id="modal_asignar_concepto" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popin">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Asignar Concepto<!-- TITULO --></h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <span hidden id="span_sociedad_id"></span>
                    <div class="row form-group">
                        <!-- CONTENIDO -->
                        <div class="col-12">
                            <label>Concepto ID:</label>
                            <select class="form-control" id="concepto_id"></select>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12" id="msgAsignarConcepto"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-alt-primary" id="guardar_concepto">
                    <i class="fa fa-check"></i> Guardar
                </button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        get_tbl_sociedades();
        obtener_conceptos_sel();

        $('#btn_sociedades').click(function () {
            obtener_sociedades();
        });

        $('body').on('click', '#asignar_concepto', function(){
            const id = $(this).closest('tr').attr('data-id');
            $('#span_sociedad_id').text(id);
            $('#modal_asignar_concepto').modal();
        });

        $('#guardar_concepto').click(function(){
            const sociedad_id = $('#span_sociedad_id').text();
            const concepto_id = parseInt($('#concepto_id option:selected').val()) || 0;
            var comp_error = $('#msgAsignarConcepto');
            if(concepto_id == 0){
                comp_error.html(send_danger_msg('Error: Debe seleccionar un concepto.'));
                return;
            }
            const obj = {
                opc : 'update_sociedad_concepto',
                id : sociedad_id,
                concepto_id : concepto_id
            }
            send_request('logic/sociedades_logic.php', obj, comp_error, function(){
                $('#modal_asignar_concepto').modal('hide');
            })
        });

        $('#modal_asignar_concepto').on('hidden.bs.modal', function(){
           $('#span_sociedad_id').text('');
           $('#concepto_id').val(0);
        });
    });
    function get_tbl_sociedades() {
        const obj = {
            opc : 'get_tbl_sociedades'
        }
        send_request('logic/sociedades_logic.php', obj, '#tbl_sociedades');
    }

    function obtener_sociedades(){
        const obj = {
            opc : 'get_sociedades'
        }
        send_request('logic/sociedades_logic.php', obj, '#tbl_sociedades');
    }

    function obtener_conceptos_sel(){
        const obj = {
            opc : 'conceptos_sel'
        }
        send_request('logic/conceptos_logic.php', obj, '#concepto_id');
    }
</script>

<?php
 utilities::load_template_footer($cb);

?>
