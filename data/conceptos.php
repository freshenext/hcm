<?php

require_once __DIR__ . '/conexion.php';
require_once __DIR__ . '/../query/conceptos_query.php';

Class conceptos extends conexion {

    public static function get_conceptos(){
        $stm = self::preparar_sentencia(conceptos_query::get_conceptos());
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function insert_concepto($descripcion, $mandt, $sprsl, $infty, $subty){
        $stm = self::preparar_sentencia(conceptos_query::insert_concepto());
        $stm->bindParam(":descripcion", $descripcion);
        $stm->bindParam(":mandt", $mandt);
        $stm->bindParam(":sprsl", $sprsl);
        $stm->bindParam(':infty',$infty);
        $stm->bindParam(':subty',$subty);
        $stm->execute();
    }

    public static function update_concepto_vacacional($id){
        $stm = self::preparar_sentencia(conceptos_query::update_concepto_vacacional());
        $stm->bindParam(':id', $id, PDO::PARAM_INT);
        $stm->execute();
    }
}