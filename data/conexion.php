<?php
//require_once __DIR__ . '/../logica/config.php';
Class conexion {

    function __construct()
    {
        $this->conn = $this->obtener_conexion();
    }

    private static function obtener_conexion(){
        //Producción
        $host = 'box2067.bluehost.com';
        $db = 'dtinspec_hcm';
        $port = '3306';
        $username = 'dtinspec_francis';
        $password = 'Okguy123';
        $charset = 'utf8';
        $host_db_port = 'mysql:host=' . $host . ':' . $port . ';dbname=' . $db . ';charset=' . $charset . ';';
        $options = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION);
        try {
            $pdo = new PDO($host_db_port,$username,$password, $options);
            return $pdo;
        } catch (PDOException $ex){
            echo $ex;
            exit(0);
        }
    }

    protected static function preparar_sentencia($sentencia){
        $pdo_object = conexion::obtener_conexion();
        $sentence = $pdo_object->prepare($sentencia);
        $sentence->pdoObj = $pdo_object;
        return $sentence;
    }

    protected static function obtener_ultimo_id_objeto_sentencia($stm){
        /* Este objeto que se va a recibir tiene un atributo pdoObj que tiene la conn  al PDO*/
        return $stm->pdoObj->lastInsertId();
    }

    protected static function obtener_filas($stm){
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }
}
