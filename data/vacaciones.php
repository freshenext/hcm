<?php

require_once __DIR__ . '/conexion.php';
require_once __DIR__ . '/../query/vacaciones_query.php';

Class vacaciones extends conexion {

    public static function get_vacaciones_by_id($id){
        $stm = self::preparar_sentencia(vacaciones_query::get_vacaciones_by_id());
        $stm->bindParam(':id',$id, PDO::PARAM_INT);
        $stm->execute();
        return self::obtener_filas($stm);
    }

    public static function get_vacaciones_activas($empleados, $es_rrhh = 0, $fecha_solicitud_creada = '', $fecha_inicio = '', $fecha_fin = ''){
        $stm = self::preparar_sentencia(vacaciones_query::get_vacaciones_activas());
        $stm->bindParam(":empleado", $empleados);
        $stm->bindParam(':es_rrhh',$es_rrhh, PDO::PARAM_INT);
        $stm->bindParam(':fecha_solicitud_creada',$fecha_solicitud_creada);
        $stm->bindParam(':fecha_inicio',$fecha_inicio);
        $stm->bindParam(':fecha_fin',$fecha_fin);
        $stm->execute();
        return self::obtener_filas($stm);
    }

    public static function insert_vacaciones($empleado_pernr, $fecha_inicio, $fecha_fin, $comentario, $dias){
        $stm = self::preparar_sentencia(vacaciones_query::insert_vacaciones());
        $stm->bindParam(":empleado_pernr", $empleado_pernr);
        $stm->bindParam(":fecha_inicio", $fecha_inicio);
        $stm->bindParam(":fecha_fin", $fecha_fin);
        $stm->bindParam(":comentario", $comentario);
        $stm->bindParam(":dias", $dias);
        $stm->execute();
        return self::obtener_ultimo_id_objeto_sentencia($stm);
    }

    public static function update_vacaciones_estado($estado, $razon, $id, $ano, $empleado_pernr_aprueba = null, $empleado_pernr_reprueba = null, $registrada_en_sap = 0){
        $stm = self::preparar_sentencia(vacaciones_query::update_vacaciones_estado());
        $stm->bindParam(":estado",$estado);
        $stm->bindParam(":razon",$razon);
        $stm->bindParam(":id",$id, PDO::PARAM_INT);
        $stm->bindParam(':ano',$ano, PDO::PARAM_INT);
        $stm->bindParam(':empleado_pernr_aprueba',$empleado_pernr_aprueba);
        $stm->bindParam(':empleado_pernr_reprueba',$empleado_pernr_reprueba);
        $stm->bindParam(':registrada_en_sap',$registrada_en_sap, PDO::PARAM_INT);
        $stm->execute();
    }

}
