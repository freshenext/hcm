<?php

require_once __DIR__ . '/../data/conexion.php';
require_once __DIR__ . '/../query/sociedades_query.php';

Class sociedades extends conexion {

    public static function get_sociedades(){
        $stm = self::preparar_sentencia(sociedades_query::get_sociedades());
        $stm->execute();
        return self::obtener_filas($stm);
    }

    public static function insert_sociedad($bukrs, $butxt){
        $stm = self::preparar_sentencia(sociedades_query::insert_sociedad());
        $stm->bindParam(':bukrs',$bukrs);
        $stm->bindParam(':butxt',$butxt);
        $stm->execute();
    }

    public static function update_sociedad_concepto($id, $concepto_id){
        $stm = self::preparar_sentencia(sociedades_query::update_sociedad_concepto());
        $stm->bindParam(':id',$id, PDO::PARAM_INT);
        $stm->bindParam(':concepto_id',$concepto_id, PDO::PARAM_INT);
        $stm->execute();
    }
}
