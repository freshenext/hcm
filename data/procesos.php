<?php
require_once __DIR__ . '/conexion.php';
require_once __DIR__ . '/../query/procesos_query.php';

Class procesos extends conexion {

    public static function getProcesos(){
        $stm = self::preparar_sentencia(procesos_query::getProcesos());
        $stm->execute();
        return self::obtener_filas($stm);
    }
    public static function insertProceso($nombre, $descripcion, $fecha_inicio, $fecha_fin){
        $stm = self::preparar_sentencia(procesos_query::insertProceso());
        $stm->bindParam(":nombre",$nombre);
        $stm->bindParam(":descripcion",$descripcion);
        $stm->bindParam(":fecha_inicio",$fecha_inicio);
        $stm->bindParam(":fecha_fin",$fecha_fin);
        $stm->execute();
        return self::obtener_ultimo_id_objeto_sentencia($stm);
    }
}