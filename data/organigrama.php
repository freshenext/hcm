<?php

require_once __DIR__ . '/../query/organigrama_query.php';

Class organigrama extends conexion {

    public static function get_vias_evaluacion(){
        $stm = self::preparar_sentencia(organigrama_query::get_vias_evaluacion());
        $stm->execute();
        return self::obtener_filas($stm);
    }
}