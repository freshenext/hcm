<?php

require_once __DIR__ . '/conexion.php';
require_once __DIR__ . '/../query/empleados_query.php';

Class empleados extends conexion {

    public static function get_empleados($email, $pass){
        $stm = self::preparar_sentencia(empleados_query::get_empleados());
        $stm->bindParam(":email",$email);
        $stm->bindParam(":pass",$pass);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function get_empleados_modo_prueba($email){
        $stm = self::preparar_sentencia(empleados_query::get_empleados_modo_prueba());
        $stm->bindParam(":email",$email);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }


    public static function get_all_empleados(){
        $stm = self::preparar_sentencia(empleados_query::get_all_empleados());
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function insert_empleados($bukrs = null, $butxt = null, $pernr = null, $entrydate = null, $leavingdate = null, $ename = null, $molga = null, $formofaddress = null, $firstname = null, $lastname = null, $nameatbirth = null, $secondname = null, $middlename = null, $fullname = null, $knownas = null, $academicgrade = null, $secondacadgrade = null, $aristrocratictitle = null, $surnameprefix = null, $secondnameprefix = null, $initials = null, $gender = null, $dateofbirth = null, $birthplace = null, $stateofbirth = null, $countryofbirth = null, $maritalstatus = null, $maritalstatussince = null, $numberofchildren = null, $religion = null, $language = null, $languageiso = null, $nationality = null, $secondnationality = null, $thirdnationality = null, $idnumber = null, $nameofformofaddress = null, $nameofgender = null, $nameofstateofbirth = null, $werks = null, $werkstext = null, $btrtl = null, $btrtltext = null, $persg = null, $persgtext = null, $persk = null, $persktext = null, $abkrs = null, $abkrstext = null, $ansvh = null, $ansvhtext = null, $kokrs = null, $kostl = null, $kostltext = null, $orgeh = null, $orgehtext = null, $stell = null, $stelltext = null, $plans = null, $planstext = null, $cpind = null, $trfar = null, $trfartext = null, $trfgb = null, $trfgbtext = null, $trfgr = null, $trfst = null, $bsgrd = null, $email = null, $username = null, $betrg = null, $anzhl = null, $docid = null, $passport = null, $ssn = null, $drive = null, $addresstype = null, $coname = null, $streetandhouseno = null, $scndaddressline = null, $city = null, $district = null, $postalcodecity = null, $state = null, $country = null, $telephonenumber = null, $nameofaddresstype = null, $nameofstate = null, $nameofcountry = null, $datum = null, $motpr = null, $tprog = null, $tagty = null, $ftkla = null, $varia = null, $tpkla = null, $zmodn = null, $stdaz = null, $activ = null, $nat01 = null, $nat02 = null, $pamod = null, $paymentmethod = null, $payee = null, $bankcountry = null, $bankkey = null, $accountno = null, $nameofbanktype = null, $nameofpaymentmethod = null, $nameofcurrency = null, $ctedt = null, $stat2 = null){
        $stm = self::preparar_sentencia(empleados_query::insert_empleados());
        $stm->bindParam(':bukrs', $bukrs, PDO::PARAM_STR);
        $stm->bindParam(':butxt', $butxt, PDO::PARAM_STR);
        $stm->bindParam(':pernr', $pernr, PDO::PARAM_INT);
        $stm->bindParam(':entrydate', $entrydate, PDO::PARAM_STR);
        $stm->bindParam(':leavingdate', $leavingdate, PDO::PARAM_STR);
        $stm->bindParam(':ename', $ename, PDO::PARAM_STR);
        $stm->bindParam(':molga', $molga, PDO::PARAM_STR);
        $stm->bindParam(':formofaddress', $formofaddress, PDO::PARAM_STR);
        $stm->bindParam(':firstname', $firstname, PDO::PARAM_STR);
        $stm->bindParam(':lastname', $lastname, PDO::PARAM_STR);
        $stm->bindParam(':nameatbirth', $nameatbirth, PDO::PARAM_STR);
        $stm->bindParam(':secondname', $secondname, PDO::PARAM_STR);
        $stm->bindParam(':middlename', $middlename, PDO::PARAM_STR);
        $stm->bindParam(':fullname', $fullname, PDO::PARAM_STR);
        $stm->bindParam(':knownas', $knownas, PDO::PARAM_STR);
        $stm->bindParam(':academicgrade', $academicgrade, PDO::PARAM_STR);
        $stm->bindParam(':secondacadgrade', $secondacadgrade, PDO::PARAM_STR);
        $stm->bindParam(':aristrocratictitle', $aristrocratictitle, PDO::PARAM_STR);
        $stm->bindParam(':surnameprefix', $surnameprefix, PDO::PARAM_STR);
        $stm->bindParam(':secondnameprefix', $secondnameprefix, PDO::PARAM_STR);
        $stm->bindParam(':initials', $initials, PDO::PARAM_STR);
        $stm->bindParam(':gender', $gender, PDO::PARAM_STR);
        $stm->bindParam(':dateofbirth', $dateofbirth, PDO::PARAM_STR);
        $stm->bindParam(':birthplace', $birthplace, PDO::PARAM_STR);
        $stm->bindParam(':stateofbirth', $stateofbirth, PDO::PARAM_STR);
        $stm->bindParam(':countryofbirth', $countryofbirth, PDO::PARAM_STR);
        $stm->bindParam(':maritalstatus', $maritalstatus, PDO::PARAM_STR);
        $stm->bindParam(':maritalstatussince', $maritalstatussince, PDO::PARAM_STR);
        $stm->bindParam(':numberofchildren', $numberofchildren, PDO::PARAM_STR);
        $stm->bindParam(':religion', $religion, PDO::PARAM_STR);
        $stm->bindParam(':language', $language, PDO::PARAM_STR);
        $stm->bindParam(':languageiso', $languageiso, PDO::PARAM_STR);
        $stm->bindParam(':nationality', $nationality, PDO::PARAM_STR);
        $stm->bindParam(':secondnationality', $secondnationality, PDO::PARAM_STR);
        $stm->bindParam(':thirdnationality', $thirdnationality, PDO::PARAM_STR);
        $stm->bindParam(':idnumber', $idnumber, PDO::PARAM_STR);
        $stm->bindParam(':nameofformofaddress', $nameofformofaddress, PDO::PARAM_STR);
        $stm->bindParam(':nameofgender', $nameofgender, PDO::PARAM_STR);
        $stm->bindParam(':nameofstateofbirth', $nameofstateofbirth, PDO::PARAM_STR);
        $stm->bindParam(':werks', $werks, PDO::PARAM_STR);
        $stm->bindParam(':werkstext', $werkstext, PDO::PARAM_STR);
        $stm->bindParam(':btrtl', $btrtl, PDO::PARAM_STR);
        $stm->bindParam(':btrtltext', $btrtltext, PDO::PARAM_STR);
        $stm->bindParam(':persg', $persg, PDO::PARAM_STR);
        $stm->bindParam(':persgtext', $persgtext, PDO::PARAM_STR);
        $stm->bindParam(':persk', $persk, PDO::PARAM_STR);
        $stm->bindParam(':persktext', $persktext, PDO::PARAM_STR);
        $stm->bindParam(':abkrs', $abkrs, PDO::PARAM_STR);
        $stm->bindParam(':abkrstext', $abkrstext, PDO::PARAM_STR);
        $stm->bindParam(':ansvh', $ansvh, PDO::PARAM_STR);
        $stm->bindParam(':ansvhtext', $ansvhtext, PDO::PARAM_STR);
        $stm->bindParam(':kokrs', $kokrs, PDO::PARAM_STR);
        $stm->bindParam(':kostl', $kostl, PDO::PARAM_STR);
        $stm->bindParam(':kostltext', $kostltext, PDO::PARAM_STR);
        $stm->bindParam(':orgeh', $orgeh, PDO::PARAM_STR);
        $stm->bindParam(':orgehtext', $orgehtext, PDO::PARAM_STR);
        $stm->bindParam(':stell', $stell, PDO::PARAM_STR);
        $stm->bindParam(':stelltext', $stelltext, PDO::PARAM_STR);
        $stm->bindParam(':plans', $plans, PDO::PARAM_STR);
        $stm->bindParam(':planstext', $planstext, PDO::PARAM_STR);
        $stm->bindParam(':cpind', $cpind, PDO::PARAM_STR);
        $stm->bindParam(':trfar', $trfar, PDO::PARAM_STR);
        $stm->bindParam(':trfartext', $trfartext, PDO::PARAM_STR);
        $stm->bindParam(':trfgb', $trfgb, PDO::PARAM_STR);
        $stm->bindParam(':trfgbtext', $trfgbtext, PDO::PARAM_STR);
        $stm->bindParam(':trfgr', $trfgr, PDO::PARAM_STR);
        $stm->bindParam(':trfst', $trfst, PDO::PARAM_STR);
        $stm->bindParam(':bsgrd', $bsgrd, PDO::PARAM_STR);
        $stm->bindParam(':email', $email, PDO::PARAM_STR);
        $stm->bindParam(':username', $username, PDO::PARAM_STR);
        $stm->bindParam(':betrg', $betrg, PDO::PARAM_STR);
        $stm->bindParam(':anzhl', $anzhl, PDO::PARAM_STR);
        $stm->bindParam(':docid', $docid, PDO::PARAM_STR);
        $stm->bindParam(':passport', $passport, PDO::PARAM_STR);
        $stm->bindParam(':ssn', $ssn, PDO::PARAM_STR);
        $stm->bindParam(':drive', $drive, PDO::PARAM_STR);
        $stm->bindParam(':addresstype', $addresstype, PDO::PARAM_STR);
        $stm->bindParam(':coname', $coname, PDO::PARAM_STR);
        $stm->bindParam(':streetandhouseno', $streetandhouseno, PDO::PARAM_STR);
        $stm->bindParam(':scndaddressline', $scndaddressline, PDO::PARAM_STR);
        $stm->bindParam(':city', $city, PDO::PARAM_STR);
        $stm->bindParam(':district', $district, PDO::PARAM_STR);
        $stm->bindParam(':postalcodecity', $postalcodecity, PDO::PARAM_STR);
        $stm->bindParam(':state', $state, PDO::PARAM_STR);
        $stm->bindParam(':country', $country, PDO::PARAM_STR);
        $stm->bindParam(':telephonenumber', $telephonenumber, PDO::PARAM_STR);
        $stm->bindParam(':nameofaddresstype', $nameofaddresstype, PDO::PARAM_STR);
        $stm->bindParam(':nameofstate', $nameofstate, PDO::PARAM_STR);
        $stm->bindParam(':nameofcountry', $nameofcountry, PDO::PARAM_STR);
        $stm->bindParam(':datum', $datum, PDO::PARAM_STR);
        $stm->bindParam(':motpr', $motpr, PDO::PARAM_STR);
        $stm->bindParam(':tprog', $tprog, PDO::PARAM_STR);
        $stm->bindParam(':tagty', $tagty, PDO::PARAM_STR);
        $stm->bindParam(':ftkla', $ftkla, PDO::PARAM_STR);
        $stm->bindParam(':varia', $varia, PDO::PARAM_STR);
        $stm->bindParam(':tpkla', $tpkla, PDO::PARAM_STR);
        $stm->bindParam(':zmodn', $zmodn, PDO::PARAM_STR);
        $stm->bindParam(':stdaz', $stdaz, PDO::PARAM_STR);
        $stm->bindParam(':activ', $activ, PDO::PARAM_STR);
        $stm->bindParam(':nat01', $nat01, PDO::PARAM_STR);
        $stm->bindParam(':nat02', $nat02, PDO::PARAM_STR);
        $stm->bindParam(':pamod', $pamod, PDO::PARAM_STR);
        $stm->bindParam(':paymentmethod', $paymentmethod, PDO::PARAM_STR);
        $stm->bindParam(':payee', $payee, PDO::PARAM_STR);
        $stm->bindParam(':bankcountry', $bankcountry, PDO::PARAM_STR);
        $stm->bindParam(':bankkey', $bankkey, PDO::PARAM_STR);
        $stm->bindParam(':accountno', $accountno, PDO::PARAM_STR);
        $stm->bindParam(':nameofbanktype', $nameofbanktype, PDO::PARAM_STR);
        $stm->bindParam(':nameofpaymentmethod', $nameofpaymentmethod, PDO::PARAM_STR);
        $stm->bindParam(':nameofcurrency', $nameofcurrency, PDO::PARAM_STR);
        $stm->bindParam(':ctedt', $ctedt, PDO::PARAM_STR);
        $stm->bindParam(':stat2', $stat2, PDO::PARAM_STR);
        $stm->execute();
    }

}