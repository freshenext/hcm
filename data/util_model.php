<?php

require_once __DIR__ . '/conexion.php';
require_once __DIR__ . '/../query/util_query.php';

Class util_model extends conexion {

    public static function get_columnas_activas(){
        $stm = self::preparar_sentencia(util_query::get_columnas_activas());
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }


}