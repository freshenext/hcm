<?php

require_once __DIR__ . '/conexion.php';
require_once __DIR__ . '/../query/categorias_query.php';

Class categorias extends conexion {

    public static function get_categorias(){
        $stm = self::preparar_sentencia(categorias_query::get_categorias());
        $stm->execute();
        return self::obtener_filas($stm);
    }

    public static function insert_categorias($nombre, $orden, $activo, $icono = null){
        $stm = self::preparar_sentencia(categorias_query::insert_categoria());
        $stm->bindParam(':nombre',$nombre);
        $stm->bindParam(':orden',$orden);
        $stm->bindParam(':activo',$activo, PDO::PARAM_INT);
        $stm->bindParam(':icono',$icono);
        $stm->execute();
        return conexion::obtener_ultimo_id_objeto_sentencia($stm);
    }

    public static function update_categorias($id, $nombre, $orden, $activo, $icono = null){
        $stm = self::preparar_sentencia(categorias_query::update_categoria());
        $stm->bindParam(':id',$id);
        $stm->bindParam(':nombre',$nombre);
        $stm->bindParam(':orden',$orden);
        $stm->bindParam(':activo',$activo, PDO::PARAM_INT);
        $stm->bindParam(':icono',$icono);
        $stm->execute();
    }

    public static function get_opciones_categoria($categoria){
        $stm = self::preparar_sentencia(categorias_query::get_opciones_categoria());
        $stm->bindParam(":categoria_id", $categoria);
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function insert_opcion_categoria($opcion_id, $categoria_id, $orden){
        $stm = self::preparar_sentencia(categorias_query::insert_opcion_categoria());
        $stm->bindParam(':opcion_id',$opcion_id, PDO::PARAM_INT);
        $stm->bindParam(':categoria_id',$categoria_id, PDO::PARAM_INT);
        $stm->bindParam(':orden',$orden, PDO::PARAM_INT);
        $stm->execute();
        conexion::obtener_ultimo_id_objeto_sentencia($stm);
    }

    public static function update_opcion_categoria($id, $orden){
        $stm = self::preparar_sentencia(categorias_query::update_opcion_categoria());
        $stm->bindParam(':id',$id);
        $stm->bindParam(':orden',$orden);
        $stm->execute();
    }

    public static function delete_opcion_categoria($id){
        $stm = self::preparar_sentencia(categorias_query::delete_opcion_categoria());
        $stm->bindParam(':id',$id);
        $stm->execute();
    }

    public static function delete_categorias($id){
        $stm = self::preparar_sentencia(categorias_query::delete_categoria());
        $stm->bindParam(':id',$id);
        $stm->execute();
    }

    public static function get_iconos(){
        $stm = self::preparar_sentencia(categorias_query::get_iconos());
        $stm->execute();
        return self::obtener_filas($stm);
    }

}