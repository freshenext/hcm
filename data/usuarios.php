<?php

require_once __DIR__ . '/../data/conexion.php';
require_once  __DIR__ . '/../query/usuarios_query.php';

Class usuarios extends conexion {

    public static function get_usuarios(){
        $stm = self::preparar_sentencia(usuarios_query::get_usuarios());
        $stm->execute();
        return self::obtener_filas($stm);
    }

    public static function get_empleado_roles($empleado_id){
        $stm = self::preparar_sentencia(usuarios_query::get_empleado_roles());
        $stm->bindParam(':empleado_id',$empleado_id);
        $stm->execute();
        return self::obtener_filas($stm);
    }

    public static function insert_empleado_rol($rol_id, $empleado_id){
        $stm = self::preparar_sentencia(usuarios_query::insert_empleado_rol());
        $stm->bindParam(':rol_id',$rol_id);
        $stm->bindParam(':empleado_id',$empleado_id);
        $stm->execute();
    }

    public static function delete_empleado_rol($id){
        $stm = self::preparar_sentencia(usuarios_query::delete_empleado_rol());
        $stm->bindParam(':id',$id);
        $stm->execute();
    }

    public static function get_empleado_opciones($empleado_id){
        $stm = self::preparar_sentencia(usuarios_query::get_empleado_opciones());
        $stm->bindParam(':empleado_id',$empleado_id, PDO::PARAM_INT);
        $stm->execute();
        return self::obtener_filas($stm);
    }

    public static function get_empleado_categorias($empleado_id){
        $stm = self::preparar_sentencia(usuarios_query::get_empleado_categorias());
        $stm->bindParam(':empleado_id',$empleado_id, PDO::PARAM_INT);
        $stm->execute();
        return self::obtener_filas($stm);
    }
}