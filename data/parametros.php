<?php
require_once __DIR__ . '/conexion.php';
require_once __DIR__ . '/../query/parametros_query.php';

Class parametros extends conexion {

    public static function get_parametros(){
        $stm = self::preparar_sentencia(parametros_query::get_parametros());
        $stm->execute();
        return self::obtener_filas($stm);
    }

    public static function insert_parametros($imagen_puesto, $imagen_personal, $imagen_empresa){
        $stm = self::preparar_sentencia(parametros_query::insert_parametros());
        $stm->bindParam(':imagen_puesto',$imagen_puesto);
        $stm->bindParam(':imagen_personal',$imagen_personal);
        $stm->bindParam(':imagen_empresa',$imagen_empresa);
        $stm->execute();
    }

    public static function validar_parametros_existe(){
        $stm = self::preparar_sentencia(parametros_query::validar_parametros_existe());
        $stm->execute();
        return self::obtener_filas($stm);
    }

    public static function update_parametros($imagen_puesto, $imagen_personal, $imagen_empresa, $id){
        $stm = self::preparar_sentencia(parametros_query::update_parametros());
        $stm->bindParam(':imagen_puesto',$imagen_puesto);
        $stm->bindParam(':imagen_personal',$imagen_personal);
        $stm->bindParam(':imagen_empresa',$imagen_empresa);
        $stm->bindParam(':id',$id);
        $stm->execute();
    }
}
