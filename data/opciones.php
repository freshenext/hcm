<?php


require_once __DIR__ . '/conexion.php';
require_once __DIR__ . '/../query/opciones_query.php';

Class opciones extends conexion {

    public static function get_opciones(){
        $stm = self::preparar_sentencia(opciones_query::get_opciones());
        $stm->execute();
        return $stm->fetchAll(PDO::FETCH_ASSOC);
    }

    public static function insert_opcion($descripcion, $link, $activado){
        $stm = self::preparar_sentencia(opciones_query::insert_opcion());
        $stm->bindParam(':descripcion',$descripcion);
        $stm->bindParam(':link',$link);
        $stm->bindParam(':activado',$activado);
        $stm->execute();
        return self::obtener_ultimo_id_objeto_sentencia($stm);
    }

    public static function update_opcion($descripcion, $link, $activado, $id){
        $stm = self::preparar_sentencia(opciones_query::update_opcion());
        $stm->bindParam(':descripcion',$descripcion);
        $stm->bindParam(':link',$link);
        $stm->bindParam(':activado',$activado);
        $stm->bindParam(':id',$id);
        $stm->execute();
        return self::obtener_ultimo_id_objeto_sentencia($stm);
    }


}