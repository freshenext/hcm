<?php

require_once __DIR__ . '/conexion.php';
require_once  __DIR__ . '/../query/roles_query.php';

Class roles extends conexion {

    public static function get_roles(){
        $stm = self::preparar_sentencia(roles_query::get_roles());
        $stm->execute();
        return self::obtener_filas($stm);
    }

    public static function insert_rol($desc, $activo, $es_rrhh){
        $stm = self::preparar_sentencia(roles_query::insert_rol());
        $stm->bindParam(':desc',$desc);
        $stm->bindParam(':activo',$activo, PDO::PARAM_INT);
        $stm->bindParam(':es_rrhh',$es_rrhh, PDO::PARAM_INT);
        $stm->execute();
    }

    public static function update_rol($desc, $activo, $id, $es_rrhh){
        $stm = self::preparar_sentencia(roles_query::update_rol());
        $stm->bindParam(':desc',$desc);
        $stm->bindParam(':activo',$activo, PDO::PARAM_INT);
        $stm->bindParam(':id',$id, PDO::PARAM_INT);
        $stm->bindParam(':es_rrhh',$es_rrhh, PDO::PARAM_INT);
        $stm->execute();
    }

    public static function get_roles_opc($rol_id){
        $stm = self::preparar_sentencia(roles_query::get_roles_opc());
        $stm->bindParam(':rol_id',$rol_id, PDO::PARAM_INT);
        $stm->execute();
        return self::obtener_filas($stm);
    }

    public static function save_roles_opc($rol_id, $opcion_id){
        $stm = self::preparar_sentencia(roles_query::save_roles_opc());
        $stm->bindParam(':rol_id',$rol_id, PDO::PARAM_INT);
        $stm->bindParam(':opcion_id',$opcion_id, PDO::PARAM_INT);
        $stm->execute();
    }

    public static function delete_rol_opc($id){
        $stm = self::preparar_sentencia(roles_query::delete_rol_opc());
        $stm->bindParam(':id',$id, PDO::PARAM_INT);
        $stm->execute();
    }

    public static function get_opciones_categoria(){
        $stm = self::preparar_sentencia(roles_query::get_opciones_categoria());
        $stm->execute();
        return self::obtener_filas($stm);
    }
}
