<?php

require_once __DIR__ . '/conexion.php';
require_once __DIR__ . '/../query/perfiles_query.php';

Class perfiles extends conexion {
    public static function get_perfiles(){
        $stm = self::preparar_sentencia(perfiles_query::get_perfiles());
        $stm->execute();
        return self::obtener_filas($stm);
    }

    public static function insert_perfil($descripcion, $activo = 1){
        $stm = self::preparar_sentencia(perfiles_query::insert_perfil());
        $stm->bindParam(':descripcion',$descripcion);
        $stm->bindParam(':activo',$activo, PDO::PARAM_INT);
        $stm->execute();
    }

    public static function update_perfil($id, $descripcion, $activo){
        $stm = self::preparar_sentencia(perfiles_query::update_perfil());
        $stm->bindParam(':id',$id);
        $stm->bindParam(':descripcion',$descripcion);
        $stm->bindParam(':activo',$activo, PDO::PARAM_INT);
        $stm->execute();
    }

    public static function delete_perfil($id){
        $stm = self::preparar_sentencia(perfiles_query::delete_perfil());
        $stm->bindParam(':id',$id, PDO::PARAM_INT);
        $stm->execute();
    }

    public static function get_perfil_rol($perfil_id){
        $stm = self::preparar_sentencia(perfiles_query::get_perfil_rol());
        $stm->bindParam(':perfil_id',$perfil_id);
        $stm->execute();
        return self::obtener_filas($stm);
    }

    public static function get_roles(){
        $stm = self::preparar_sentencia(perfiles_query::get_roles());
        $stm->execute();
        return self::obtener_filas($stm);
    }

}