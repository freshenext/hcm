<?php require 'inc/_global/config.php'; ?>
<?php require 'inc/_global/views/head_start.php'; ?>
<?php require 'inc/_global/views/head_end.php'; ?>
<?php require 'inc/_global/views/page_start.php'; ?>
<script src="/assets/js/core/jquery.min.js"></script>
<script src="/logic/utl.js"></script>
<!-- Page Content -->
<div class="bg-body-dark bg-pattern" style="background-image: url('<?php echo $cb->assets_folder; ?>/media/various/bg-pattern-inverse.png');">
    <div class="row mx-0 justify-content-center">
        <div class="hero-static col-lg-6 col-xl-4">
            <div class="content content-full overflow-hidden">
                <!-- Header -->
                <div class="py-30 text-center">
                    <img src="/images/hcm.png">
                    <h2 class="h5 font-w400 text-muted mb-0">La forma revolucionaria de manejar tus datos!</h2>
                </div>
                <!-- END Header -->

                <!-- Sign In Form -->
                <!-- jQuery Validation functionality is initialized with .js-validation-signin class in js/pages/op_auth_signin.min.js which was auto compiled from _es6/pages/op_auth_signin.js -->
                <!-- For more examples you can check out https://github.com/jzaefferer/jquery-validation -->
                <script src="/assets/_es6/pages/op_auth_signin.js"></script>
                <form class="js-validation-signin animated fadeInDownBig" id="form_login" action="logic/model_login.php" method="POST">
                    <div class="block block-themed block-rounded block-shadow">
                        <div class="block-header bg-gd-aqua">
                            <h3 class="block-title">Inicio de Sesión</h3>
                            <div class="block-options">
                                <button type="button" class="btn-block-option">
                                    <i class="si si-wrench"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <input type="text" value="login" name="opc" hidden>
                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="login-email">Correo Electrónico</label>
                                    <input type="email" class="form-control" id="login-email" name="login-email">
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-12">
                                    <label for="login-password">Contraseña</label>
                                    <input type="password" class="form-control" id="login-password" name="login-password">
                                </div>
                            </div>
                            <div id="wrong_creds" class="form-group row" <?php if(isset($_GET['f']) && $_GET['f'] == '1'){} else { echo 'hidden'; } ?>>
                                <div class="col-12">
                                    <button type="button" class="btn btn-danger btn-block">
                                        <i class="fa fa-times mr-5"></i>¡Error! Favor verificar credenciales.
                                    </button>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-sm-6 d-sm-flex align-items-center push">
                                    <div class="custom-control custom-checkbox mr-auto ml-0 mb-0">
                                        <input type="checkbox" class="custom-control-input" id="login-remember-me" name="login-remember-me">
                                        <label class="custom-control-label" for="login-remember-me">Recuerdame</label>
                                    </div>
                                </div>
                                <div class="col-sm-6 text-sm-right push">
                                    <button type="submit" class="btn btn-alt-primary">
                                        <i class="si si-login mr-10"></i> Iniciar Sesión
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="block-content bg-body-light">
                            <div class="form-group text-center">
                                <a class="link-effect text-muted mr-10 mb-5 d-inline-block" href="op_auth_reminder3.php">
                                    <i class="fa fa-warning mr-5"></i> ¿Olvidó su contraseña?
                                </a>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END Sign In Form -->
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->
<script>
    $('#form_login').submit(function () {
        $('#wrong_creds').remove();
    });
</script>
<?php require 'inc/_global/views/page_end.php'; ?>
<?php require 'inc/_global/views/footer_start.php'; ?>

<!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/jquery-validation/jquery.validate.js'); ?>

<!-- Page JS Code -->
<?php $cb->get_js('assets/_es6/pages/op_auth_signin.js'); ?>

<?php require 'inc/_global/views/footer_end.php'; ?>