<?php
require_once __DIR__ . '/logic/utl.php';

$cb = utilities::load_template();
?>

<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="block block-themed">
                <div class="block-header">
                    <h3 class="block-title">Usuarios</h3>
                </div>
                <div class="block-content">
                    <div class="row form-group">
                        <!-- USUARIOS BUSQUEDA -->
                        <div class="col-12 form-group">
                            <label>Buscar: </label>
                            <input id="txt_busqueda" type="text" placeholder="Ingresar nombre del empleado" class="form-control">
                        </div>
                        <div class="col-12 form-group">
                            <button type="button" class="btn btn-primary pull-right" id="buscar_usuarios"><b>BUSCAR</b> <i class="fa fa-search"></i></button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <!-- USUARIOS OUTPUT -->
                        <div class="col-12" id="tbl_usuarios">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_empleado_roles" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popin">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Roles del Empleado<!-- TITULO --></h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <span id="empleado_id" hidden></span>
                    <div class="row">
                        <div class="col-12 form-group">
                            <label>Agregar rol:</label>
                            <select class="form-control" id="sel_rol_empleado"></select>
                        </div>
                        <div class="col-12 form-group">
                            <button type="button" class="btn btn-primary" id="agregar_rol_a_empleado">Agregar <i class="fa fa-plus"></i></button>
                        </div>
                        <div class="col-12 form-group" id="msgAddRol">

                        </div>
                    </div>
                    <div class="row">
                        <!-- CONTENIDO -->
                        <div class="col-12" id="tbl_roles_empleado"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-alt-primary">
                    <i class="fa fa-check"></i> Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<script>

    $('#buscar_usuarios').click(function () {
        buscar_usuarios();
    });

    $(document).ready(function(){
        buscar_usuarios();
        populate_sel_empleado_rol();
    });
    function buscar_usuarios(){
        const texto = $('#txt_busqueda').val();
        const obj = {
            opc : 'buscar_usuarios',
            texto : texto
        }
        send_request('logic/usuarios_logic.php', obj, '#tbl_usuarios');
    }

    $('body').on('click', '#ver_empleado_roles', function(){
        $('#empleado_id').text($(this).closest('tr').attr('data-id'));
        get_empleado_roles();
        $('#modal_empleado_roles').modal();
    });

    $('#agregar_rol_a_empleado').click(function(){
        const rol_id = parseInt($('#sel_rol_empleado option:selected').val()) || 0;
        const empleado_id = parseInt($('#empleado_id').text());
        if(rol_id == 0){
            return;
        }

        const obj = {
            opc : 'add_rol_to_empleado',
            empleado_id : empleado_id,
            rol_id : rol_id
        }
        send_request('logic/usuarios_logic.php', obj, '#msgAddRol', function(){
            get_empleado_roles();
        });
    });

    $('body').on('click','#borrar_rol_empleado',function(){
        const id = $(this).closest('tr').attr('data-id');
        const obj = {
            opc : 'borrar_rol_empleado',
            id : id
        }
        send_request('logic/usuarios_logic.php', obj, '#msgAddRol', function(){
            get_empleado_roles();
        });
    });

    function populate_sel_empleado_rol(){
        const obj = {
            opc : 'get_roles'
        }
        send_request('logic/perfiles_logic.php', obj, '#sel_rol_empleado')
    }

    function get_empleado_roles(){
        const comp = $('#tbl_roles_empleado');
        const empleado_id = $('#empleado_id').text();
        const obj = {
            opc : 'get_empleado_roles',
            empleado_id : empleado_id
        }
        send_request('logic/usuarios_logic.php', obj, comp);
    }
</script>

<?php
    utilities::load_template_footer($cb);
?>
