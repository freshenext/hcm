<?php require_once __DIR__ . '/logic/utl.php';
$cb = utilities::load_template();
?>
    <!-- Page Content -->
    <div class="content">
        <div class="row">
            <div class="col-12">
                <div class="block block-themed">
                    <div class="block-header">
                        <h3 class="block-title">Cargar conceptos</h3>
                    </div>
                    <div class="block-content">
                        <div class="row form-group">
                            <div class="col-12">
                                <div class="text-center">
                                    <button id="cargar_conceptos" type="button" class="btn btn-primary">
                                    Cargar Conceptos
                                    </button>
                                </div>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12" id="msg_load_conceptos">

                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-12">
                                <span></span>
                            </div>
                            <div class="col-12" id="tbl_conceptos">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Page Content -->


<script>
    function get_conceptos_if_exist(){
        var obj = {
            opc : 'get_conceptos'
        }
        send_request('logic/conceptos_logic.php', obj, '#tbl_conceptos');
    }

    function get_conceptos_from_sap(){
        var obj = {
            opc : 'get_sap_conceptos'
        }

        send_request('logic/conceptos_logic.php', obj, '#msg_load_conceptos');
    }

    function make_concepto_vacacional(id){
        var obj = {
            opc : 'make_concepto_vacacional',
            id : id
        }
        send_request('logic/conceptos_logic.php', obj, '#tbl_conceptos');
    }
    $(document).ready(function () {
        get_conceptos_if_exist();

        $('#cargar_conceptos').click(function () {
            get_conceptos_from_sap();
        });

        $('body').off('click','[name=btn_set_vacaciones]').on('click','[name=btn_set_vacaciones]', function () {
            var id = $(this).closest('tr').attr('data-id');
            make_concepto_vacacional(id);
        })

    });
</script>


<?php utilities::load_template_footer($cb); ?>