<?php

require_once __DIR__ . '/../data/empleados.php';
require_once __DIR__ . '/../logic/utl.php';
//Descripcion: Modelo que inicia las sesiones.

$opc = utilities::check_post_opc();

if($opc == 'login'){
    $email = filter_var($_POST['login-email'], FILTER_SANITIZE_EMAIL);
    $pass = filter_var($_POST['login-password'], FILTER_SANITIZE_STRING);

    //Valida que exista
    if(utilities::get_modo() == 1){
        $empleado = empleados::get_empleados_modo_prueba($email);
    } else {
        $empleado = empleados::get_empleados($email ,$pass);
    }


    if(count($empleado) == 1){
        //Exitoso
        //Creacion de sesion
        $empleado = $empleado[0];
        $nombre = $empleado['firstname'];
        $apellido = $empleado['lastname'];
        $numero_empleado = $empleado['pernr'];
        $id = $empleado['id'];
        $es_rrhh = (int) $empleado['es_rrhh'];
        utilities::create_session($nombre, $apellido, $email, $numero_empleado, $id, $es_rrhh);
        //Obtener empleados debajo de usuario
        utilities::get_session_sub_empleados();
        //Envio del usuario al index
        utilities::redirect_to_url('');
    } else {
        //No exitoso, retornar a login con error.
        utilities::redirect_to_url('login.php?f=1');
    }
}



?>