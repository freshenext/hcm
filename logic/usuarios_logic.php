<?php

require_once __DIR__ . '/../logic/utl.php';
require_once __DIR__ . '/../data/usuarios.php';

$opc = utilities::check_post_opc();


if($opc == 'buscar_usuarios'){

    $texto = isset($_POST['texto']) ? filter_var($_POST['texto'], FILTER_SANITIZE_STRING) : '';

    $usuarios = usuarios::get_usuarios();

    $tabla = "<table class='table table-striped'><thead>
                    <th>Nombre</th>
                    <th>Sociedad</th>
                    <th>Empleado ID</th>
                    <th></th>
                </thead><tbody>";

    $btn_roles = '<button type="button" class="btn btn-success" id="ver_empleado_roles">VER ROLES <i class="fa fa-search"></i></button>';
    foreach($usuarios as $usuario){

        $tabla .= "<tr data-id='{$usuario['id']}'>
                <td>{$usuario['ename']}</td>
                <td>{$usuario['butxt']}</td>
                <td>{$usuario['pernr']}</td>
                <td>{$btn_roles}</td>
        </tr>";
    }

    $tabla .= "</tbody></table>";
    echo $tabla;


} else if ($opc == 'get_empleado_roles'){
    $empleado_id = isset($_POST['empleado_id']) ? (int) filter_var($_POST['empleado_id'], FILTER_SANITIZE_NUMBER_INT) : '';
    $empleado_roles = usuarios::get_empleado_roles($empleado_id);

    $tabla = "<table class='table table-striped'><thead>
                    <th>Rol</th>
                    <th></th>
                </thead><tbody>";

    $btn_borrar = "<button type='button' class='btn btn-danger' id='borrar_rol_empleado'>Borrar <i class='fa fa-remove'></i></button>";
    foreach($empleado_roles as $empleado_rol){
        $tabla .= "<tr data-id='{$empleado_rol['id']}'>
                        <td>{$empleado_rol['rol_desc']}</td>
                        <td>{$btn_borrar}</td>
                    </tr>";
    }
    $tabla .= "</tbody></table";

    echo $tabla;
} else if ($opc == 'add_rol_to_empleado'){
    $empleado_id = isset($_POST['empleado_id']) ? (int) filter_var($_POST['empleado_id'], FILTER_SANITIZE_NUMBER_INT) : '';
    $rol_id = isset($_POST['rol_id']) ? (int) filter_var($_POST['rol_id'], FILTER_SANITIZE_NUMBER_INT) : '';


    usuarios::insert_empleado_rol($rol_id, $empleado_id);
    echo utilities::swal_success('', 'Rol insertado de forma correcta.');
} else if ($opc == 'borrar_rol_empleado'){
    $id = isset($_POST['id']) ? (int) filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT) : '';

    usuarios::delete_empleado_rol($id);
    echo utilities::swal_success('','Rol borrado de forma correcta.');
}