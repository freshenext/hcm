<?php

require_once __DIR__ . '/../logic/utl.php';
require_once __DIR__ . '/../data/conceptos.php';

$opc = isset($_POST['opc']) ? $_POST['opc'] : '';

if($opc == 'get_conceptos'){

    $tbl = conceptos::get_conceptos();

    //Ya existen conceptos
    if(count($tbl) > 0){
        //Crear tabla para los conceptos
        $tabla = '<table class="table table-striped">
                                    <thead>
                                        <tr style="font-weight: bold;">
                                            <td>Concepto</td>
                                            <td>Es vacaciones</td>
                                            <td>Activo</td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody id="tbl_conceptos">

                                    ';
        $button_es_vacaciones = '<button class="btn btn-info" name="btn_set_vacaciones" style="font-weight: bold;">Definir como vacaciones <i class="si si-pencil"></i></button>';
        foreach ($tbl as $concepto) {
            $id = $concepto['id'];
            $descripcion = $concepto['descripcion'];
            $es_vacaciones = $concepto['vacaciones_desc'];
            $activo = $concepto['activo_desc'];

            $tabla .= '<tr data-id="' . $id . '">
                <td>' . $descripcion . '</td>
                <td>' . $es_vacaciones . '</td>
                <td>' . $activo . '</td>
                <td class="text-center">' . $button_es_vacaciones . '</td>
            </tr>';
        }

        $tabla .= '</tbody>
                    </table>';
        echo $tabla;
    } else {
        //No existen, no hacer nada
        echo utilities::send_info_msg('No hay conceptos.');
    }
} else if ($opc == 'get_sap_conceptos'){
    //Si ya existen, entonces no insertar.
    $conceptos = conceptos::get_conceptos();
    if(count($conceptos) > 0){
        echo utilities::send_danger_msg('Error! Ya han sido cargados los conceptos.');
        return;
    }

    $xml = utilities::request_xml(soap_queries::get_conceptos());
    $xml = utilities::make_xml_as_json($xml, '//item');

    foreach ($xml as $concepto) {
        //Estos conceptos ya son traidos desde el web service
        $descripcion = $concepto['Stext'];
        $mandt = $concepto['Mandt'];//
        $sprsl = $concepto['Sprsl'];//
        $infty = $concepto['Infty'];
        $subty = $concepto['Subty'];

        //Insertar en base de datos
        conceptos::insert_concepto($descripcion, $mandt, $sprsl, $infty, $subty);
    }
    echo utilities::swal_success('Éxito!', 'Conceptos cargados de forma exitosa.');
    echo "<script>get_conceptos_if_exist();</script>";
} else if ($opc == 'make_concepto_vacacional'){
    $id = isset($_POST['id']) ? filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT) : 0;

    //Si el ID no es igual a cero, ejecutar query
    if($id != 0){
        conceptos::update_concepto_vacacional($id);
        //LISTO
        echo "<script>get_conceptos_if_exist();</script>";
    } else {
        //Excepcion
    }
} else if ($opc == 'conceptos_sel'){
    //Buscar conceptos
    $conceptos = conceptos::get_conceptos();

    $options = "<option value='0'>Seleccionar</option>";
    foreach($conceptos as $concepto){
        //Para cada uno, agregar al select
        $options .= "<option value='{$concepto['id']}'>{$concepto['descripcion']}</option>";
    }

    echo $options;
}