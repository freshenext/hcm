<?php

require_once __DIR__ . '/../logic/utl.php';
require_once __DIR__ . '/../data/opciones.php';

$opc = utilities::check_post_opc();

if($opc == 'get_opciones'){
    //Obtener las opciones en una tabla.
    //Query para obtener los queries
    $opciones = opciones::get_opciones();

    //Esa tabla debe tener dos botones EDITAR Y ELIMINAR
    $tabla = "<table class='table table-striped'>
                <thead>
                    <th>Descripción</th>
                    <th>Link</th>
                    <th>Activada</th>
                    <th>Acciones</th>
                </thead><tbody>";

    //Para cada opcion
    foreach ($opciones as $opcion) {
        $btn_edit = utilities::get_btn_edit('', 'opc_editar', 'EDITAR', true);
        $btn_del = utilities::get_btn_delete('', 'opc_eliminar', 'ELIMINAR', true);

        $tabla .= "<tr data-id='{$opcion['id']}'>
                        <td class='descripcion'>{$opcion['descripcion']}</td>
                        <td class='link'>{$opcion['link']}</td>
                        <td class='activada' data-bool='{$opcion['activo']}'>{$opcion['activada']}</td>
                        <td>
                            {$btn_edit}
                            {$btn_del}
                        </td>
                    </tr>";
    }

    $tabla .= "</tbody></table>";
    echo $tabla;

} else if ($opc == 'save_opcion'){
    $id = isset($_POST['id']) ? (int) filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT) : 0;
    $descripcion = isset($_POST['descripcion']) ?  filter_var($_POST['descripcion'], FILTER_SANITIZE_STRING) : '';
    $link = isset($_POST['link']) ? filter_var($_POST['link'], FILTER_SANITIZE_STRING) : '';
    $activado = isset($_POST['activado']) ? filter_var($_POST['activado'], FILTER_SANITIZE_NUMBER_INT) : '';

    try {
        if($id == 0){
            //Insert
            opciones::insert_opcion($descripcion, $link, $activado);
            utilities::swal_success('Éxito!', 'Nueva opción creada de forma exitosa.');
        } else {
            //Update
            opciones::update_opcion($descripcion, $link, $activado, $id);
            utilities::swal_success('Éxito!', 'Opción actualizada de forma exitosa.');
        }
    } catch (Exception $ex){
        utilities::swal_error('Error!','Hubo un error: ' . $ex->getMessage());
    }


} else if ($opc == 'get_sel_opciones'){
    $opciones = opciones::get_opciones();
    $options = utilities::build_select($opciones, 'id', 'descTodo');

    echo $options;
}
