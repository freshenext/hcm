<?php
require_once __DIR__ . '/../logic/utl.php';
require_once __DIR__ . '/../data/empleados.php';
require_once __DIR__ . '/../data/vacaciones.php';

$opc = isset($_POST['opc']) ? $_POST['opc'] : '';


if($opc == 'solicitar_vacaciones'){
    $empleado_id = ''; //Aqui debe ser empleado logeado $_SESSION
    $fecha_inicio = filter_var($_POST['fecha_inicio'], FILTER_SANITIZE_STRING); //Fecha en la cual inicia las vacaciones YYYY-MM-DD
    $fecha_fin = filter_var($_POST['fecha_fin'], FILTER_SANITIZE_STRING); //Fecha en la cual retorna de vacaciones YYYY-MM-DD
    $concepto_id = ''; //Concepto, normalmente es el de vacaciones
    $comentario = isset($_POST['comentario']) ? filter_var($_POST['comentario'], FILTER_SANITIZE_STRING) : '';

    //Get dias disfrute
    $dias_disfrute = utilities::request_xml(soap_queries::get_dias_disfrute($fecha_inicio,$fecha_fin));

    //XML, buscar ExDays
    $dias = $dias_disfrute->xpath('//ExDays')[0];
    $dias = json_decode(json_encode($dias),true)[0];
    $dias = (int) $dias;
    //Asignar fecha desde, fecha hasta, dias, en $_SESSION
    $arreglo_vacaciones = array(
        'fecha_inicio' => $fecha_inicio,
        'fecha_fin' => $fecha_fin,
        'dias' => $dias,
        'comentario' => $comentario
    );
    $_SESSION['vacaciones'] = $arreglo_vacaciones;
    //Enviar script SWAL que confirme la accion
    echo '<script>
            Swal.fire({
                title : "Tiene '.$dias.' días de vacaciones",
                text : "¿Está seguro que desea registrar la solicitud?",
                icon : "warning",
                showCancelButton : true,
                confirmButtonText : "Si",
                cancelButtonText : "No"
            }).then((result) => {
                if(result.value){
                    save_vacation();
                }
            });
        </script>';

} else if ($opc == 'get_sap_empleados'){
    $tbl_empleados = empleados::get_all_empleados();
    if(count($tbl_empleados) > 0){
        echo utilities::send_danger_msg('Error! Ya los empleados han sido cargados.');
        return;
    }

    $xml = utilities::request_xml(soap_queries::get_empleado());
    $xml = json_decode(json_encode($xml->xpath('//item')), true);

    foreach ($xml as $key => $empleado) {
        //Convertir keys a lower case
        $empleado = array_change_key_case($empleado, CASE_LOWER);

        //Definir variables
        $bukrs = isset($empleado['bukrs']) ? FILTER_VAR($empleado['bukrs'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $butxt = isset($empleado['butxt']) ? FILTER_VAR($empleado['butxt'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $pernr = isset($empleado['pernr']) ? FILTER_VAR($empleado['pernr'], FILTER_SANITIZE_NUMBER_INT, FILTER_NULL_ON_FAILURE) : null;
        $entrydate = isset($empleado['entrydate']) ? FILTER_VAR($empleado['entrydate'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $leavingdate = isset($empleado['leavingdate']) ? FILTER_VAR($empleado['leavingdate'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $ename = isset($empleado['ename']) ? FILTER_VAR($empleado['ename'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $molga = isset($empleado['molga']) ? FILTER_VAR($empleado['molga'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $formofaddress = isset($empleado['formofaddress']) ? FILTER_VAR($empleado['formofaddress'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $firstname = isset($empleado['firstname']) ? FILTER_VAR($empleado['firstname'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $lastname = isset($empleado['lastname']) ? FILTER_VAR($empleado['lastname'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $nameatbirth = isset($empleado['nameatbirth']) ? FILTER_VAR($empleado['nameatbirth'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $secondname = isset($empleado['secondname']) ? FILTER_VAR($empleado['secondname'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $middlename = isset($empleado['middlename']) ? FILTER_VAR($empleado['middlename'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $fullname = isset($empleado['fullname']) ? FILTER_VAR($empleado['fullname'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $knownas = isset($empleado['knownas']) ? FILTER_VAR($empleado['knownas'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $academicgrade = isset($empleado['academicgrade']) ? FILTER_VAR($empleado['academicgrade'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $secondacadgrade = isset($empleado['secondacadgrade']) ? FILTER_VAR($empleado['secondacadgrade'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $aristrocratictitle = isset($empleado['aristrocratictitle']) ? FILTER_VAR($empleado['aristrocratictitle'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $surnameprefix = isset($empleado['surnameprefix']) ? FILTER_VAR($empleado['surnameprefix'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $secondnameprefix = isset($empleado['secondnameprefix']) ? FILTER_VAR($empleado['secondnameprefix'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $initials = isset($empleado['initials']) ? FILTER_VAR($empleado['initials'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $gender = isset($empleado['gender']) ? FILTER_VAR($empleado['gender'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $dateofbirth = isset($empleado['dateofbirth']) ? FILTER_VAR($empleado['dateofbirth'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $birthplace = isset($empleado['birthplace']) ? FILTER_VAR($empleado['birthplace'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $stateofbirth = isset($empleado['stateofbirth']) ? FILTER_VAR($empleado['stateofbirth'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $countryofbirth = isset($empleado['countryofbirth']) ? FILTER_VAR($empleado['countryofbirth'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $maritalstatus = isset($empleado['maritalstatus']) ? FILTER_VAR($empleado['maritalstatus'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $maritalstatussince = isset($empleado['maritalstatussince']) ? FILTER_VAR($empleado['maritalstatussince'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $numberofchildren = isset($empleado['numberofchildren']) ? FILTER_VAR($empleado['numberofchildren'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $religion = isset($empleado['religion']) ? FILTER_VAR($empleado['religion'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $language = isset($empleado['language']) ? FILTER_VAR($empleado['language'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $languageiso = isset($empleado['languageiso']) ? FILTER_VAR($empleado['languageiso'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $nationality = isset($empleado['nationality']) ? FILTER_VAR($empleado['nationality'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $secondnationality = isset($empleado['secondnationality']) ? FILTER_VAR($empleado['secondnationality'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $thirdnationality = isset($empleado['thirdnationality']) ? FILTER_VAR($empleado['thirdnationality'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $idnumber = isset($empleado['idnumber']) ? FILTER_VAR($empleado['idnumber'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $nameofformofaddress = isset($empleado['nameofformofaddress']) ? FILTER_VAR($empleado['nameofformofaddress'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $nameofgender = isset($empleado['nameofgender']) ? FILTER_VAR($empleado['nameofgender'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $nameofstateofbirth = isset($empleado['nameofstateofbirth']) ? FILTER_VAR($empleado['nameofstateofbirth'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $werks = isset($empleado['werks']) ? FILTER_VAR($empleado['werks'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $werkstext = isset($empleado['werkstext']) ? FILTER_VAR($empleado['werkstext'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $btrtl = isset($empleado['btrtl']) ? FILTER_VAR($empleado['btrtl'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $btrtltext = isset($empleado['btrtltext']) ? FILTER_VAR($empleado['btrtltext'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $persg = isset($empleado['persg']) ? FILTER_VAR($empleado['persg'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $persgtext = isset($empleado['persgtext']) ? FILTER_VAR($empleado['persgtext'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $persk = isset($empleado['persk']) ? FILTER_VAR($empleado['persk'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $persktext = isset($empleado['persktext']) ? FILTER_VAR($empleado['persktext'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $abkrs = isset($empleado['abkrs']) ? FILTER_VAR($empleado['abkrs'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $abkrstext = isset($empleado['abkrstext']) ? FILTER_VAR($empleado['abkrstext'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $ansvh = isset($empleado['ansvh']) ? FILTER_VAR($empleado['ansvh'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $ansvhtext = isset($empleado['ansvhtext']) ? FILTER_VAR($empleado['ansvhtext'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $kokrs = isset($empleado['kokrs']) ? FILTER_VAR($empleado['kokrs'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $kostl = isset($empleado['kostl']) ? FILTER_VAR($empleado['kostl'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $kostltext = isset($empleado['kostltext']) ? FILTER_VAR($empleado['kostltext'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $orgeh = isset($empleado['orgeh']) ? FILTER_VAR($empleado['orgeh'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $orgehtext = isset($empleado['orgehtext']) ? FILTER_VAR($empleado['orgehtext'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $stell = isset($empleado['stell']) ? FILTER_VAR($empleado['stell'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $stelltext = isset($empleado['stelltext']) ? FILTER_VAR($empleado['stelltext'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $plans = isset($empleado['plans']) ? FILTER_VAR($empleado['plans'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $planstext = isset($empleado['planstext']) ? FILTER_VAR($empleado['planstext'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $cpind = isset($empleado['cpind']) ? FILTER_VAR($empleado['cpind'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $trfar = isset($empleado['trfar']) ? FILTER_VAR($empleado['trfar'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $trfartext = isset($empleado['trfartext']) ? FILTER_VAR($empleado['trfartext'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $trfgb = isset($empleado['trfgb']) ? FILTER_VAR($empleado['trfgb'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $trfgbtext = isset($empleado['trfgbtext']) ? FILTER_VAR($empleado['trfgbtext'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $trfgr = isset($empleado['trfgr']) ? FILTER_VAR($empleado['trfgr'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $trfst = isset($empleado['trfst']) ? FILTER_VAR($empleado['trfst'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $bsgrd = isset($empleado['bsgrd']) ? FILTER_VAR($empleado['bsgrd'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $email = isset($empleado['email']) ? FILTER_VAR($empleado['email'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $username = isset($empleado['username']) ? FILTER_VAR($empleado['username'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $betrg = isset($empleado['betrg']) ? FILTER_VAR($empleado['betrg'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $anzhl = isset($empleado['anzhl']) ? FILTER_VAR($empleado['anzhl'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_NULL_ON_FAILURE) : null;
        $docid = isset($empleado['docid']) ? FILTER_VAR($empleado['docid'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $passport = isset($empleado['passport']) ? FILTER_VAR($empleado['passport'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $ssn = isset($empleado['ssn']) ? FILTER_VAR($empleado['ssn'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $drive = isset($empleado['drive']) ? FILTER_VAR($empleado['drive'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $addresstype = isset($empleado['addresstype']) ? FILTER_VAR($empleado['addresstype'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $coname = isset($empleado['coname']) ? FILTER_VAR($empleado['coname'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $streetandhouseno = isset($empleado['streetandhouseno']) ? FILTER_VAR($empleado['streetandhouseno'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $scndaddressline = isset($empleado['scndaddressline']) ? FILTER_VAR($empleado['scndaddressline'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $city = isset($empleado['city']) ? FILTER_VAR($empleado['city'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $district = isset($empleado['district']) ? FILTER_VAR($empleado['district'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $postalcodecity = isset($empleado['postalcodecity']) ? FILTER_VAR($empleado['postalcodecity'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $state = isset($empleado['state']) ? FILTER_VAR($empleado['state'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $country = isset($empleado['country']) ? FILTER_VAR($empleado['country'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $telephonenumber = isset($empleado['telephonenumber']) ? FILTER_VAR($empleado['telephonenumber'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $nameofaddresstype = isset($empleado['nameofaddresstype']) ? FILTER_VAR($empleado['nameofaddresstype'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $nameofstate = isset($empleado['nameofstate']) ? FILTER_VAR($empleado['nameofstate'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $nameofcountry = isset($empleado['nameofcountry']) ? FILTER_VAR($empleado['nameofcountry'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $datum = isset($empleado['datum']) ? FILTER_VAR($empleado['datum'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $motpr = isset($empleado['motpr']) ? FILTER_VAR($empleado['motpr'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $tprog = isset($empleado['tprog']) ? FILTER_VAR($empleado['tprog'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $tagty = isset($empleado['tagty']) ? FILTER_VAR($empleado['tagty'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $ftkla = isset($empleado['ftkla']) ? FILTER_VAR($empleado['ftkla'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $varia = isset($empleado['varia']) ? FILTER_VAR($empleado['varia'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $tpkla = isset($empleado['tpkla']) ? FILTER_VAR($empleado['tpkla'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $zmodn = isset($empleado['zmodn']) ? FILTER_VAR($empleado['zmodn'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $stdaz = isset($empleado['stdaz']) ? FILTER_VAR($empleado['stdaz'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $activ = isset($empleado['activ']) ? FILTER_VAR($empleado['activ'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $nat01 = isset($empleado['nat01']) ? FILTER_VAR($empleado['nat01'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $nat02 = isset($empleado['nat02']) ? FILTER_VAR($empleado['nat02'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $pamod = isset($empleado['pamod']) ? FILTER_VAR($empleado['pamod'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $paymentmethod = isset($empleado['paymentmethod']) ? FILTER_VAR($empleado['paymentmethod'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $payee = isset($empleado['payee']) ? FILTER_VAR($empleado['payee'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $bankcountry = isset($empleado['bankcountry']) ? FILTER_VAR($empleado['bankcountry'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $bankkey = isset($empleado['bankkey']) ? FILTER_VAR($empleado['bankkey'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $accountno = isset($empleado['accountno']) ? FILTER_VAR($empleado['accountno'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $nameofbanktype = isset($empleado['nameofbanktype']) ? FILTER_VAR($empleado['nameofbanktype'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $nameofpaymentmethod = isset($empleado['nameofpaymentmethod']) ? FILTER_VAR($empleado['nameofpaymentmethod'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $nameofcurrency = isset($empleado['nameofcurrency']) ? FILTER_VAR($empleado['nameofcurrency'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $ctedt = isset($empleado['ctedt']) ? FILTER_VAR($empleado['ctedt'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
        $stat2 = isset($empleado['stat2']) ? FILTER_VAR($empleado['stat2'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;

        //Insertar en método
        empleados::insert_empleados($bukrs, $butxt, $pernr, $entrydate, $leavingdate, $ename, $molga, $formofaddress, $firstname, $lastname, $nameatbirth, $secondname, $middlename, $fullname, $knownas, $academicgrade, $secondacadgrade, $aristrocratictitle, $surnameprefix, $secondnameprefix, $initials, $gender, $dateofbirth, $birthplace, $stateofbirth, $countryofbirth, $maritalstatus, $maritalstatussince, $numberofchildren, $religion, $language, $languageiso, $nationality, $secondnationality, $thirdnationality, $idnumber, $nameofformofaddress, $nameofgender, $nameofstateofbirth, $werks, $werkstext, $btrtl, $btrtltext, $persg, $persgtext, $persk, $persktext, $abkrs, $abkrstext, $ansvh, $ansvhtext, $kokrs, $kostl, $kostltext, $orgeh, $orgehtext, $stell, $stelltext, $plans, $planstext, $cpind, $trfar, $trfartext, $trfgb, $trfgbtext, $trfgr, $trfst, $bsgrd, $email, $username, $betrg, $anzhl, $docid, $passport, $ssn, $drive, $addresstype, $coname, $streetandhouseno, $scndaddressline, $city, $district, $postalcodecity, $state, $country, $telephonenumber, $nameofaddresstype, $nameofstate, $nameofcountry, $datum, $motpr, $tprog, $tagty, $ftkla, $varia, $tpkla, $zmodn, $stdaz, $activ, $nat01, $nat02, $pamod, $paymentmethod, $payee, $bankcountry, $bankkey, $accountno, $nameofbanktype, $nameofpaymentmethod, $nameofcurrency, $ctedt, $stat2);
    }
    echo utilities::send_success_msg('Éxito! Empleados insertados de forma exitosa.');
    /*
    ----------------------------
    QUERY PARA obtener las variables limpias
    ----------------------------

    SELECT concat('$', COLUMN_NAME, ' = isset($empleado[''', COLUMN_NAME, ''']) ? FILTER_VAR($empleado[''', COLUMN_NAME, '''], ',TIPO,') : null;') as variable_php
    FROM (select column_name,
           case data_type
            when 'varchar' then 'FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE'
            when 'int' then 'FILTER_SANITIZE_NUMBER_INT, FILTER_NULL_ON_FAILURE'
            when 'double' then 'FILTER_SANITIZE_NUMBER_FLOAT, FILTER_NULL_ON_FAILURE'
            when 'datetime' then 'FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE'
           end AS TIPO
    from information_schema.columns
    where table_name = 'empleados'
      and column_name != 'id') T

    ------------------------------
    QUERY PARA INSERTAR LAS VARIABLES EN EL METODO
    ------------------------------
SET @@session.group_concat_max_len = @@global.max_allowed_packet;
SELECT group_concat(concat('$',column_name, ' = null') separator ', ') as variable_for_param
FROM (select column_name,
       case data_type
        when 'varchar' then 'FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE'
        when 'int' then 'FILTER_SANITIZE_NUMBER_INT, FILTER_NULL_ON_FAILURE'
        when 'double' then 'FILTER_SANITIZE_NUMBER_FLOAT, FILTER_NULL_ON_FAILURE'
        when 'datetime' then 'FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE'
       end AS TIPO
from information_schema.columns
where table_name = 'empleados'
  and column_name != 'id') T

    ----------------------------
    Query para crear INSERT
    ----------------------------

SET @@session.group_concat_max_len = @@global.max_allowed_packet;
SELECT concat(
    'insert into empleados (',
    group_concat(column_name separator ', '),
    ')'
    ,
    concat('VALUES (',group_concat(concat(':',column_name) separator ', '), ')'))
    as todos
FROM (select column_name,
       case data_type
        when 'varchar' then 'FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE'
        when 'int' then 'FILTER_SANITIZE_NUMBER_INT, FILTER_NULL_ON_FAILURE'
        when 'double' then 'FILTER_SANITIZE_NUMBER_FLOAT, FILTER_NULL_ON_FAILURE'
        when 'datetime' then 'FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE'
       end AS TIPO
from information_schema.columns
where table_name = 'empleados'
  and column_name != 'id') T

    ----------------------
    Query para obtener los bind param
    ----------------------
    SET @@session.group_concat_max_len = @@global.max_allowed_packet;
SELECT group_concat(concat('$stm->bindParam('':',column_name,''', $',COLUMN_NAME,', ',TIPO,');') separator '\n')
FROM (select column_name,
       case data_type
        when 'varchar' then 'PDO::PARAM_STR'
        when 'int' then 'PDO::PARAM_INT'
        when 'double' then 'PDO::PARAM_STR'
        when 'datetime' then 'PDO::PARAM_STR'
       end AS TIPO
from information_schema.columns
where table_name = 'empleados'
  and column_name != 'id') T

    ----------------------
    Query para obtener el insert del modelo
    ----------------------

    SELECT group_concat(concat('$',column_name) separator ', ') as variable_for_param
FROM (select column_name,
       case data_type
        when 'varchar' then 'FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE'
        when 'int' then 'FILTER_SANITIZE_NUMBER_INT, FILTER_NULL_ON_FAILURE'
        when 'double' then 'FILTER_SANITIZE_NUMBER_FLOAT, FILTER_NULL_ON_FAILURE'
        when 'datetime' then 'FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE'
       end AS TIPO
from information_schema.columns
where table_name = 'empleados'
  and column_name != 'id') T

    */
} else if ($opc == 'get_empleados'){
    $empleados = empleados::get_all_empleados();

    if(count($empleados) > 0){
        //Existen
        $tabla = '<table class="table">
                    <thead>
                    <tr>
                        <td>
                            Nombre del Empleado
                        </td>
                    </tr>
                    </thead>';
        foreach ($empleados as $empleado){
            $nombre_completo = $empleado['firstname'] . ' ' . $empleado['lastname'];

        }
    }
} else if ($opc == 'guardar_vacaciones'){
    //Utilizar la variable $_SESSION para guardar las vacaciones
    $fecha_inicio = $_SESSION['vacaciones']['fecha_inicio'];
    $fecha_fin = $_SESSION['vacaciones']['fecha_fin'];
    $comentario = $_SESSION['vacaciones']['comentario'];
    $dias = $_SESSION['vacaciones']['dias'];
    $pernr = $_SESSION['empleado_id'];
    //Luego de terminar ese proceso, mandar SWAL y limpiar fechas, y txt comentarios
    $id = vacaciones::insert_vacaciones($pernr, $fecha_inicio, $fecha_fin, $comentario, $dias);

    echo "<script>
                $('#fecha_inicio, #fecha_fin, #comentario').val('')
                    Swal.fire({
                        title : \"¡Éxito!\",
                        text : \"Sus vacaciones han sido solicitadas.\",
                        type : \"success\"
                    });
        </script>";
} else if ($opc == 'get_historico_vacaciones'){
    //Obtener XML que trae el historico de vacaciones
    $xml = utilities::request_xml(soap_queries::get_dias_disfrutados($_SESSION['empleado_id']));

    //Obtener los items
    $xml = json_decode(json_encode($xml->xpath('//item')), true);

    //Crear la tabla

    $tabla = '<table class="table table-striped">
                                <thead>
                                    <tr>
                                        <th>Fecha Inicio</th>
                                        <th>Fecha Fin</th>
                                        <th style="text-align: center;">Días por defecto</th>
                                        <th style="text-align: center;">Días Disfrutados</th>
                                        <th style="text-align: center;">Días Pendientes</th>
                                        <th>Toma de vacaciones desde</th>
                                    </tr>
                                </thead>
                                <tbody>';
    //Para cada uno

    foreach($xml as $historico_vacaciones){
        $fecha_inicio = utilities::get_date_format($historico_vacaciones['Begda']);
        $fecha_fin = utilities::get_date_format($historico_vacaciones['Endda']);
        $dias_disfrute = (int) filter_var($historico_vacaciones['Anzhl'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
        $dias_disfrutados = (int) filter_var($historico_vacaciones['Kverb'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);;
        $pendientes_por_disfrutar = $dias_disfrute - $dias_disfrutados;
        $toma_vac_desde = $historico_vacaciones['Desta'];
        $toma_vac_desde_format = utilities::get_date_format($toma_vac_desde);
        $toma_vac_hasta = $historico_vacaciones['Deend'];
        $toma_vac_hasta_format = utilities::get_date_format($toma_vac_hasta);

        $tabla .= "
        <tr>
            <td>${fecha_inicio}</td>
            <td>${fecha_fin}</td>
            <td style='text-align:center;'>${dias_disfrute}</td>
            <td style='text-align:center;'>${dias_disfrutados}</td>
            <td style='text-align:center;'>${pendientes_por_disfrutar}</td>
            <td>${toma_vac_desde_format} - ${toma_vac_hasta_format}</td>
        </tr>";
    }
    $tabla .= '</tbody>
                            </table>';
    echo $tabla;

}