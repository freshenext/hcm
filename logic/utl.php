<?php

require_once __DIR__ . '/../data/util_model.php';
require_once __DIR__ . '/../query/soap_requests.php';
require_once __DIR__ . '/../data/parametros.php';

Class utilities {

    public static function redirect_to_url($url){
        header("Location: /${url}");
    }

    public static function get_modo(){
        //Aqui se obtiene el modo, si es prueba o produccion
        if(strpos($_SERVER['HTTP_HOST'], 'localhost') !== false){
          //Es localhost
            return 1;
        } else {
            return 0;
        }
    }

    public static function check_post_opc(){
        if(isset($_POST['opc']))
            return $_POST['opc'];
        else
            return 0;
    }

    public static function send_email($emails, $subject, $body){
        $link = 'http://hcm.gsuiterd.com/eapi.php'; // Link
        /* Este api recibe subject, body en HTML, y emails separados por comma*/
    }

    public static function get_active_rows(){
        $columnas_activas = util_model::get_columnas_activas();
    }

    public static function request_xml($soap_card = '', $timeout = 10){
        $opciones = array('login' => 'jcollado', 'password' => 'abaper2230');
        $link = 'http://10.16.60.71:8000/sap/bc/srt/rfc/sap/zhcm_on_cloud/100/zhcm_on_cloud/data';
        $link = 'http://10.16.60.71:8000/sap/bc/srt/rfc/sap/zhcm_on_cloud/100/zhcm_on_cloud/data';
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_PORT => "8000",
            CURLOPT_URL => "http://10.16.60.71:8000/sap/bc/srt/rfc/sap/zhcm_on_cloud/100/zhcm_on_cloud/data",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => $timeout,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $soap_card,
            CURLOPT_HTTPHEADER => array(
                "Authorization: Basic amNvbGxhZG86YWJhcGVyMjIzMA==",
                "Cache-Control: no-cache",
                "Content-Type: application/soap+xml",
                "cache-control: no-cache"
            ),
        ));
        ini_set('memory_limit', '512M');
        $response = curl_exec($curl);
        curl_close($curl);
        if($response == false){
            return $response;
        }
        $response = str_replace('0000-00-00', '', $response);
        $xml_file = self::string_to_xml($response);
        return $xml_file;
    }

    public static function string_to_xml($string){
        try {
            $xml_file = new SimpleXMLElement($string);
            return $xml_file;
        } catch (Exception $exXml){
            return false;
        }

    }

    public static function make_xml_as_json($xml, $xpath = ''){
        if($xpath == ''){
            $xml = json_encode($xml);
        } else {
            $xml = json_encode($xml->xpath($xpath));
        }
        $xml = preg_replace('/:{}/', ':""', $xml);
        return json_decode($xml, true);
    }

    public static function send_success_msg($mensaje){
         return "<div class='text-center'>
            <button style='font-weight: bold;' class='btn btn-success'>
                ${mensaje}
            </button>
        </div>";
    }

    public static function send_danger_msg($mensaje){
        return "<div class='text-center'>
            <button style='font-weight: bold;' class='btn btn-danger'>
                ${mensaje}
            </button>
        </div>";
    }

    public static function send_info_msg($mensaje){
        return "<div class='text-center'>
            <button style='font-weight: bold;' class='btn btn-info'>
                ${mensaje}
            </button>
        </div>";
    }

    public static function send_warning_msg($mensaje){
        return "<div class='text-center'>
            <button style='font-weight: bold;' class='btn btn-warning'>
                ${mensaje}
            </button>
        </div>";
    }

    public static function get_session_sub_empleados(){
        //Obtener empleado ID de $_SESSION
        $xml = utilities::request_xml(soap_queries::get_sub_empleados(1000, $_SESSION['empleado_id']));
        if($xml === false){
            //Si devuelve false, no trajo nada, o no se pudo conectar
            return;
        }
        $xml = json_decode(json_encode($xml->xpath('//Employeeid')),true);
        $empleados = array();
        foreach ($xml as $empleado_id) {
            //Empujar al arreglo
            array_push($empleados, $empleado_id[0]);
        }
        //Agregar arreglo a la $_SESSION['empleados']
        $_SESSION['empleados'] = $empleados;
    }

    public static function validate_session(){
        //Validar que no esté en localhost
        $es_localhost = strpos($_SERVER['HTTP_HOST'], 'localhost') !== false ? 1 : 0;
        //Validar que no esté en el login.php
        $archivo_actual = $_SERVER['PHP_SELF'];
        if(in_array($archivo_actual, self::get_session_exceptions())){
            //Está en el login
        } else {
            //No esta en el login, validar session
            if(!isset($_SESSION)){
                //Si no está iniciada la sesión, entonces iniciarla
                session_start();
            }
            //Ahora, validar que tenga valores

            if(!isset($_SESSION['iniciada'])){
                //Si la session no tiene la key iniciada, entonces no esta logeado.
                header('Location: login.php');
                exit(0);
            }
            //De lo contrario, el usuario si está logeado.

        }
    }

    public static function get_session_exceptions(){
        return array(
            '/login.php',
            '/logic/model_login.php'
        );
    }

    public static function create_session($nombre, $apellido, $email, $empleado_pernr, $id, $es_rrhh){
        //Si no está iniciada, entonces iniciarla
        if(!isset($_SESSION)){
            session_start();
        }
        $_SESSION['nombre'] = $nombre;
        $_SESSION['apellido'] = $apellido;
        $_SESSION['email'] = $email;
        $_SESSION['empleado_id'] = $empleado_pernr;
        $_SESSION['iniciada'] = 1;
        $_SESSION['id'] = $id;
        $_SESSION['es_rrhh'] = $es_rrhh;
    }

    public static function assign_session_key($nombre_indice, $valor)
    {
        //Validar que exista la session
        if (isset($_SESSION)) {
            //Existe! Asignar key
            $_SESSION[$nombre_indice] = $valor;
        }
    }

    public static function remove_session_key($nombre_indice){
        //Validar que exista la session
        if(isset($_SESSION)){
            //Existe! Validar que el indice exista
            if(isset($_SESSION[$nombre_indice])){
                //Existe el indice! Remover
                unset($_SESSION[$nombre_indice]);
            }
        }
    }

    public static function destroy_session(){
        session_destroy();
    }

    public static function get_date_format($date){
        return date('d F Y',strtotime($date));
    }

    public static function load_template(){
        include('inc/_global/config.php');
        include('inc/backend/config.php');
        // Codebase - Page specific configuration

        $cb->l_m_content = 'narrow';
        $cb->l_header_fixed = true;
        $cb->l_header_style = '';
        $cb->l_sidebar_inverse = true;
        include('inc/_global/views/head_start.php');
        include('inc/_global/views/head_end.php');
        include('inc/_global/views/page_start.php');
        return $cb;
    }

    public static function load_template_footer($cb){
        //Este metodo tiene que recibir la clase $cb que es la que tiene lso datos de configuracion.
        //Esta la debe devolver el load_template() de utilities
        include('inc/backend/views/inc_footer.php');
        include('inc/_global/views/footer_start.php');
        include('inc/_global/views/footer_end.php');
    }

    public static function get_btn_edit($id = '', $name = '', $text = 'EDITAR', $bold = true){
        $id = $id != '' ? "id={$id}" : '';
        $name = $name != '' ? "name={$name}" : '';
        $bold = $bold == true ? "style='font-weight: bold;'" : '';
        $btn = "<button type='button' class='btn btn-info' {$id} {$name} {$bold}>{$text} <i class='fa fa-edit'></i></button>";
        return $btn;
    }

    public static function get_btn_delete($id = '', $name = '', $text = 'ELIMINAR', $bold = true){
        $id = $id != '' ? "id={$id}" : '';
        $name = $name != '' ? "name={$name}" : '';
        $bold = $bold == true ? "style='font-weight: bold;'" : '';
        $btn = "<button type='button' class='btn btn-danger' {$id} {$name} {$bold}>{$text} <i class='fa fa-trash'></i></button>";
        return $btn;
    }

    public static function get_btn_guardar($id = '', $name = '', $text = 'GUARDAR', $bold = true){
        $id = $id != '' ? "id={$id}" : '';
        $name = $name != '' ? "name={$name}" : '';
        $bold = $bold == true ? "style='font-weight: bold;'" : '';
        $btn = "<button type='button' class='btn btn-success' {$id} {$name} {$bold}>{$text} <i class='fa fa-check'></i></button>";
        return $btn;
    }


    public static function swal_success($bold = '¡Éxito!', $mensaje){
        if($bold  == '')
            $bold = '¡Éxito!';
        echo "<script>Swal.fire(
                    `{$bold}`,
                    `{$mensaje}`,
                    'success'
                );</script>";
    }

    public static function swal_danger($bold, $mensaje){
        echo "<script>Swal.fire(
                    `{$bold}`,
                    `{$mensaje}`,
                    'warning'
                );</script>";
    }

    public static function swal_error($bold, $mensaje){
        echo "<script>Swal.fire(
                    `{$bold}`,
                    `{$mensaje}`,
                    'error'
                );</script>";
    }

    public static function swal_info($bold, $mensaje){
        echo "<script>Swal.fire(
                    `{$bold}`,
                    `{$mensaje}`,
                    'info'
                );</script>";
    }

    public static function build_select($arrayOfData, $keyValueOfOption, $keyDescriptionOfOption, $extraOptions = array()){
        $options = "";
        //Si array data igual a cero, entonces retornar no hay registros
        if(count($arrayOfData) == 0){
            $options = "<option value='0'>No hay registros.</option>";
            return $options;
        } else {
            $options = "<option value='0'>Seleccionar</option>";
        }
        foreach($arrayOfData as $option){
            //Si tiene extras, entonces agregarlo
            $extraDetails ="";
            foreach($extraOptions as $extraOption){
                $nameOfAttribute = $extraOption['name'];
                $keyOfAttribute = $extraOption['key'];
                $extraDetails .= "{$nameOfAttribute}='{$option[$keyOfAttribute]}'";
            }
            $options .= "<option value='{$option[$keyValueOfOption]}' {$extraDetails}>{$option[$keyDescriptionOfOption]}</option>";
        }
        return $options;
    }

    public static function build_table_from_query($arrayOfData){
        $table = '<table class="table table-striped">';
        $thead = '<thead>';
        $tbody = '<tbody>';
        foreach($arrayOfData as $index => $data){
            $tbody .= "<tr>";
            if($index == 0){
                $thead .= '<tr>';
            }
            foreach($data as $key => $keyData){
                if($index == 0){
                    /* Si el indice = 0 entonces hay que crear el THEAD */
                    $thead .= "<th>{$key}</th>";
                }
                $tbody .= "<td name='{$key}'>{$keyData}</td>";
            }
            if($index == 0){
                $thead .= '</tr>';
            }
            $tbody .= "</tr>";
        }
        $thead .= "</thead>";
        $tbody .= "</tbody>";
        return $table . $thead . $tbody . '</table>';
    }

    public static function get_parametros_data(){
        $params = parametros::get_parametros();
        if(count($params) > 0){
            $params = $params[0];
        } else {
            $params = array();
        }
        return $params;
    }

    public static function get_image_from_blob($image, $nombre){
        /* Poner imagen en directorio temp y retornar ruta */
        $ruta = __DIR__ . '\..\temp\\' . $nombre;
        file_put_contents($ruta, $image);
        $ruta = '/temp/' . $nombre;
        return $ruta;
    }

}

utilities::validate_session();