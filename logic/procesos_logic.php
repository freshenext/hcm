<?php
require_once __DIR__ . '/../logic/utl.php';
require_once __DIR__ . '/../data/procesos.php';

$opc = utilities::check_post_opc();

if($opc == 'getProcesos'){

    $procesos = procesos::getProcesos();
    echo utilities::build_table_from_query($procesos);

} else if ($opc == 'saveProceso'){
    $id = isset($_POST['id']) ? (int) filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT) : 0;
    $nombre = isset($_POST['nombre']) ? filter_var($_POST['nombre'], FILTER_SANITIZE_STRING) : '';
    $descripcion = isset($_POST['descripcion']) ? filter_var($_POST['descripcion'], FILTER_SANITIZE_STRING) : '';
    $fecha_inicio = isset($_POST['fecha_inicio']) ? filter_var($_POST['fecha_inicio'], FILTER_SANITIZE_STRING) : '';
    $fecha_fin = isset($_POST['fecha_fin']) ? filter_var($_POST['fecha_fin'], FILTER_SANITIZE_STRING) : '';

    if($id == 0){
        $id = procesos::insertProceso($nombre, $descripcion, $fecha_inicio, $fecha_fin);
        echo utilities::swal_success('¡Éxito!', 'Proceso guardado de forma correcta.');
        echo "<script>
            getProcesos();       
            </script>";
    } else {

    }


}
