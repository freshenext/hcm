<?php

require_once __DIR__ . '/../logic/utl.php';
require_once __DIR__ . '/../data/categorias.php';


$opc = utilities::check_post_opc();

if($opc == 'get_categorias'){

    $tabla = "<table class='table table-striped'>
                <thead>
                    <th>Nombre Categoria</th>
                    <th>Orden</th>
                    <th>Icono</th>
                    <th>Activada</th>
                    <th>Acciones</th>
                </thead><tbody>";
    $categorias = categorias::get_categorias();
    foreach ($categorias as $categoria){
        $btn_edit = utilities::get_btn_edit('','edit_categoria');
        $btn_add_opc = utilities::get_btn_edit('','agregar_opc_cat','AGREGAR OPCIONES');
        $btn_del = utilities::get_btn_delete('', 'del_categoria');
        $tabla .= "<tr data-id='{$categoria['id']}' data-activo='{$categoria['activo']}' data-icono='{$categoria['icono']}'>
                        <td id='categoria_desc'>{$categoria['descripcion']}</td>
                        <td id='categoria_orden'>{$categoria['orden']}</td>
                        <td id='categoria_icono'><i class='fa {$categoria['icono']}'></i></td>
                        <td>{$categoria['activada']}</td>
                        <td>
                            {$btn_edit}
                            {$btn_add_opc}
                            {$btn_del}
                        </td>
                    </tr>";
    }
    echo $tabla;
} else if ($opc == 'save_categoria'){
    $id = isset($_POST['categoria_id']) ? (int) filter_var($_POST['categoria_id'], FILTER_SANITIZE_NUMBER_INT) : '';
    $nombre = isset($_POST['categoria_nombre']) ? filter_var($_POST['categoria_nombre'], FILTER_SANITIZE_STRING) : '';
    $orden = isset($_POST['categoria_orden']) ? (int) filter_var($_POST['categoria_orden'], FILTER_SANITIZE_NUMBER_INT) : '';
    $activada = isset($_POST['categoria_activa']) ? (int) filter_var($_POST['categoria_activa'], FILTER_SANITIZE_NUMBER_INT) : '';
    $icono = isset($_POST['categoria_icono']) ? filter_var($_POST['categoria_icono'], FILTER_SANITIZE_STRING) : '';

    try {
        if($id == 0){
            //insert
            $id = categorias::insert_categorias($nombre,$orden,$activada, $icono);
            echo utilities::swal_success('Éxito!', 'La categoria ha sido insertada de forma correcta.');
        } else {
            categorias::update_categorias($id,$nombre,$orden,$activada, $icono);
            echo utilities::swal_success('Éxito!', 'La categoria ha sido actualizada de forma correcta.');
        }

    } catch(Exception $exx) {
        echo utilities::swal_error('Error!', "Ha ocurrido un error al actualizar: {$exx->getMessage()}");
    }
} else if ($opc == 'get_categoria_opciones'){
    $categoria_id = isset($_POST['categoria_id']) ? (int) filter_var($_POST['categoria_id'], FILTER_SANITIZE_NUMBER_INT) : '';

    $opciones_de_cat = categorias::get_opciones_categoria($categoria_id);

    $tabla = "<table class='table table-bordered'><thead>
        <tr>
            <td>OPCION</td>
            <td>ORDEN</td>
            <td>ACCIONES</td>
        </tr>
    </thead><tbody>";

    foreach ($opciones_de_cat as $opcion){
        $btn_del = utilities::get_btn_delete('del_opc_cat');
        $btn_guardar_cambios = utilities::get_btn_guardar('save_opc_cat');
        $tabla .= "<tr data-id='{$opcion['id_categoria_opcion']}'>
            <td>{$opcion['descripcion']}</td>
            <td><input type='number' value='{$opcion['orden']}' class='form-control'></td>
            <td>
            <div class='text-center'>{$btn_guardar_cambios}</div>
            <div class='text-center'>{$btn_del}</div>
            </td>
        </tr>";
    }

    $tabla .= "</tbody></table>";
    echo $tabla;

} else if ($opc == 'save_opc_categoria'){
    $opcion_id = isset($_POST['opcion_id']) ? (int) filter_var($_POST['opcion_id'], FILTER_SANITIZE_NUMBER_INT) : 0;
    $categoria_id = isset($_POST['categoria_id']) ? (int) filter_var($_POST['categoria_id'], FILTER_SANITIZE_NUMBER_INT) : 0;
    $id = isset($_POST['id']) ? (int) filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT) : 0;
    $orden = isset($_POST['orden']) ? (int) filter_var($_POST['orden'], FILTER_SANITIZE_NUMBER_INT) : 0;

    try {
        if($id == 0){
            //Insert
            categorias::insert_opcion_categoria($opcion_id, $categoria_id, $orden);
            echo "<script>get_categoria_opciones();</script>";
        } else {
            //Actualizar
            categorias::update_opcion_categoria($id, $orden);
        }
        echo utilities::swal_success('¡Éxito!', 'Opción guardada de forma correcta.');
    } catch (Exception $exx){
        echo utilities::swal_error('Hubo un error.', $exx->getMessage());
    }
} else if ($opc == 'delete_categoria_opcion'){
    $id = isset($_POST['id']) ? (int) filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT) : 0;
    categorias::delete_opcion_categoria($id);
    echo utilities::swal_success('Exito!', 'Opcion borrada de categoria.');
    echo "<script>
            get_categoria_opciones();
        </script>";
} else if ($opc == 'delete_categoria'){
    $id = isset($_POST['id']) ? (int) filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT) : '';
    categorias::delete_categorias($id);
    utilities::swal_success('¡Éxito!', 'La categoría ha sido borrada.');
    echo "<script>
            load_categorias();
            </script>";
} else if ($opc == 'get_icono'){
    /* Obtener los iconos y ponerlos en un select */
    $iconos = categorias::get_iconos();

    $opts = "<option value='0'>Todos</option>";
    foreach($iconos as $icon){
        $opts .= "<option class='faFont' value='{$icon['nombreIcono']}'>{$icon['simbolo']} - {$icon['nombreIcono']}</option>";
    }

    echo $opts;
}
