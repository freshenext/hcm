<?php

require_once __DIR__ . '/../logic/utl.php';
require_once __DIR__ . '/../data/organigrama.php';


$opc = utilities::check_post_opc();

if($opc == 'get_sel_vias_evaluacion'){
    $vias_evaluacion = organigrama::get_vias_evaluacion();

    echo utilities::build_select($vias_evaluacion, 'sigla','descripcion');


} else if ($opc == 'get_organigrama'){
    /* Codigo dentro de un try para manejar todas las excepciones */
    try {
        /* Logica para crear el organigrama */

        /* Recordando, el web service toma 6 valores de entrada
            nombre_campo | desc | default
            im_plvar | variante de plan | 01
            im_otype | tipo de objeto | O
            im_objid | id objeto |
            im_begda | fecha inicio efectividad | fecha del dia
            im_endda | fecha inicio efectividad | fecha del dia
            im_wegid | via de evaluacion
        */

        $fecha = isset($_POST['fecha']) ? filter_var($_POST['fecha'], FILTER_SANITIZE_STRING) : '';
        $via_evaluacion = isset($_POST['via_evaluacion']) ? filter_var($_POST['via_evaluacion'], FILTER_SANITIZE_STRING) : '';

        /* Obtener WS */
        $ws_organigrama = utilities::request_xml(soap_queries::get_org_chart($via_evaluacion, $fecha), 60);

        /* Validar que exista */
        if($ws_organigrama === false){
            throw new Exception("El organigrama no pudo ser encontrado.");
        }
        /* Convertir xml a json*/
        $ws_organigrama = utilities::make_xml_as_json($ws_organigrama, '//item');
        /*
        Estructura de array: ({ id : id, textoAMostrar : textoAMostrar}, pariente, tooltip)
        [{'v':'1', 'f':'Joseph'}, '', 'El Presidente']
        [{'v':'2', 'f':'Francis'}, '1', 'Vice Presidente']

        El WS retorna
        Id
        Parent
        */
        $arrayGeneral = array();

        /* Obtener los parametros */
        $parametros = utilities::get_parametros_data();

        $imagen_puesto = utilities::get_image_from_blob($parametros['imagen_puesto'], 'puesto.png');
        $imagen_empresa = utilities::get_image_from_blob($parametros['imagen_empresa'], 'empresa.png');
        $imagen_personal = utilities::get_image_from_blob($parametros['imagen_personal'], 'personal.png');

        foreach($ws_organigrama as $index => $orgTest){
            $id = $orgTest['Id'];
            /* P: Persona, S: Sociedad, O: Organizacion*/
            $img = '/assets/media/avatars/avatar0.jpg';
            switch($orgTest['Otype']){
                case "O" :
                    $img = $imagen_empresa;
                    break;
                case "S":
                    $img = $imagen_puesto;
                    break;
                case "P":
                    $img = $imagen_personal;
                    break;
            }
            $parentId = $orgTest['Parent'] == '0' ? '' : $orgTest['Parent'];
            $texto = "
            <div style='width: 120px !important;'>
                <figure class='figure'>
                    <img class='img-fluid circle figure-img' src='{$img}'/>
                    <figcaption class='figure-caption text-center font-weight-bold'>
                        {$orgTest['Text4']}
                    </figcaption>
                </figure>                    
            </div>";
            $arrayVF = array('v'=> $id, 'f' => $texto);
            array_push($arrayGeneral, array($arrayVF, $parentId, $texto));
        }
        $arrayGeneral = json_encode($arrayGeneral);
        $script_js = "<script>
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Name');
            data.addColumn('string', 'Manager');
            data.addColumn('string', 'ToolTip');
    
            // For each orgchart box, provide the name, manager, and tooltip to show.
            data.addRows({$arrayGeneral});
            // Create the chart.
            var chart = new google.visualization.OrgChart(document.getElementById('div_organigrama'));
            // Draw the chart, setting the allowHtml option to true for the tooltips.
            chart.draw(data, {'allowHtml': true});
        </script>";
        //echo utilities::build_table_from_query($ws_organigrama);
        echo $script_js;
        return;
        $nodos = construirOrganigrama($ws_organigrama);
        /* Para cada padre, crear un organigrama */
        foreach($nodos as $padres){

        }

        echo $fecha . $via_evaluacion;

    } catch (Exception $ex){
        echo utilities::swal_error('Error','Error: ' . $ex->getMessage());

    }

}
exit(0);
function construirOrganigrama($arregloDenodos){
    /* Para cada uno, hay que buscar sus hijos de forma recursiva */
    $padres = array_filter($arregloDenodos, function($data) {
       return $data['Parent'] == "0";
    });
    foreach($padres as $indice => $nodos){
         $padres[$indice]['hijos'] = buscarHijos($nodos, $arregloDenodos);
    }
    return $padres;
}

function buscarHijos($nodoPrincipal, $arregloBuscar, $arrayTotal = array()){
    /*
    Del nodo actual, buscar sus hijos
        Si tiene hijos, entonces buscar esos hijos
        De lo contrario retorna esos hijos
    */
    /* Existe un campo Parent */
    $pariente_id = $nodoPrincipal['Id'];
    $hijos = array_filter($arregloBuscar, function($data) use ($pariente_id) {
        return $data['Parent'] == $pariente_id;
    });
    if(count($hijos) > 0){
        foreach($hijos as $hijo){
            $nodoPrincipal['hijos'] = buscarHijos($hijo, $arregloBuscar);
        }
    }
    return array($nodoPrincipal);
}

function construirScript($array){

}