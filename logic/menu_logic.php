<?php

require_once __DIR__ . '/utl.php';
require_once __DIR__ . '/../data/usuarios.php';

Class menu {
    public static function get_menu(){
        /* Buscar todas las opciones, de ahí filtrar con array unique por categoria id */
        $opciones = usuarios::get_empleado_opciones((int) $_SESSION['id']);

        $categorias = array_unique(array_column($opciones, 'categoria_id'));
        $categorias = array_map(function($arr) use ($opciones){
            return $opciones[$arr[0]];
        }, $categorias);
        /* Buscar todas las opciones de ese empleado */

        /* Para cada categoria, obtener su opc*/
        $opcionesArray = array();
        foreach($categorias as $categoria){
            $id_cat = $categoria['categoria_id'];
            array_push($opcionesArray, self::obtenerArregloOpcion($categoria['categoria_desc'], 'si si-cup', ''));
            $opciones = array_filter($opciones, function($opcion) use ($id_cat){
                return $id_cat == $opcion['categoria_id'];
            });
            $sub = array();
            foreach($opciones as $opcion){
                array_push($opcionesArray, self::obtenerArregloOpcion($opcion['descripcion'], 'si si-cup', $opcion['link']));
            }
        }

        return $opcionesArray;
    }

    static function obtenerArregloOpcion($descripcion, $icono = 'si si-cup', $link){
        switch($link){
            case '':
                return array(
                    'name'  => "<span class='sidebar-mini-hide'>{$descripcion}</span>",
                    'icon'  => 'si si-cup',
                    'type'   => 'heading'
                );
                break;
            default :
                return array(
                    'name'  => "<span class='sidebar-mini-hide'>{$descripcion}</span>",
                    'icon'  => 'si si-cup',
                    'url'   => $link
                );
                break;
        }

    }
}


