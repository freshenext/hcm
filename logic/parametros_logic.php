<?php
require_once __DIR__ . '/utl.php';

$opc = utilities::check_post_opc();

if($opc == 'get_parametros'){
    /* Obtener */
} else if ($opc == 'save_parametros'){
    /* Guardar */
    $id = isset($_POST['id']) ? (int) filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT) : 0;
    $imagen_puesto = null;
    $imagen_personal = null;
    $imagen_empresa = null;

    if(isset($_FILES['imgPuesto']) && $_FILES['imgPuesto']['error'] == 0){
        $imagen_puesto = file_get_contents($_FILES['imgPuesto']['tmp_name']);
    }
    if(isset($_FILES['imgEmpleado']) && $_FILES['imgEmpleado']['error'] == 0){
        $imagen_personal = file_get_contents($_FILES['imgEmpleado']['tmp_name']);
    }
    if(isset($_FILES['imgEmpresa']) && $_FILES['imgEmpresa']['error'] == 0){
        $imagen_empresa = file_get_contents($_FILES['imgEmpresa']['tmp_name']);
    }

    /* Validar en parametros que no exista */
    $existe = parametros::validar_parametros_existe()[0]['existe'];
    if($existe > 0){
        $id = 1;
    }


    if($id == 0){
        parametros::insert_parametros($imagen_puesto, $imagen_personal, $imagen_empresa);
    } else {
        parametros::update_parametros($imagen_puesto, $imagen_personal, $imagen_empresa, $id);
    }
}