<?php

require_once __DIR__ . '/utl.php';
require_once __DIR__ . '/../data/vacaciones.php';

$opc = utilities::check_post_opc();

if($opc == 'get_emp_vacaciones'){
    /* Declaracion de variables de fecha */
    $fecha_solicitud_creada = isset($_POST['fecha_solicitud_creada']) ? filter_var($_POST['fecha_solicitud_creada'], FILTER_SANITIZE_STRING) : '';
    $fecha_inicio = isset($_POST['fecha_inicio']) ? filter_var($_POST['fecha_inicio'], FILTER_SANITIZE_STRING) : '';
    $fecha_fin = isset($_POST['fecha_fin']) ? filter_var($_POST['fecha_fin'], FILTER_SANITIZE_STRING) : '';
    //Primero, detectar si la persona logeada es de recursos humanos o simplemente un encargado
    //1-Flow supervisor
    //La variable session tiene el empleado pernr
    $empleados = '';
    if(is_array($_SESSION['empleados'])){
        /* Hacer implode con |*/
        $empleados = implode('|', $_SESSION['empleados']);
    }

    //2-Flow recursos humanos
    //El rol de recursos humanos tiene el empleado
    $es_rrhh = $_SESSION['es_rrhh'];
    //Obtener tabla de vacaciones
    //Desde la bd, tabla vacaciones
    $vacaciones_tbl = vacaciones::get_vacaciones_activas($empleados, $es_rrhh, $fecha_solicitud_creada, $fecha_inicio, $fecha_fin);
    $tabla = '<table class="table table-striped">
                <thead>
                    <tr>
                        <th>Empleado</th>
                        <th>Departamento</th>
                        <th>Fecha Inicio</th>
                        <th>Fecha Fin</th>
                        <th>Estado</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>';
    foreach ($vacaciones_tbl as $vacaciones) {
        $fecha_inicio = utilities::get_date_format($vacaciones['fecha_inicio']);
        $fecha_fin = utilities::get_date_format($vacaciones['fecha_fin']);
        $btn_ver = utilities::get_btn_edit('btn_ver_empleado', '', 'VER EMPLEADO');
        $btn_aprobar = utilities::get_btn_guardar('btn_aprobar_vac', '', 'APROBAR');
        $btn_rechazar = utilities::get_btn_delete('btn_rechazar_vac', '', 'RECHAZAR');
        //Crear fila con información para cada vacación
        $tabla .= "<tr data-id='{$vacaciones['id']}'>
                    <td class='td_nombre'>{$vacaciones['ename']}</td>
                    <td class='td_organizacion'>{$vacaciones['organizacion']}</td>
                    <td class='td_fecha_inicio'>{$fecha_inicio}</td>
                    <td class='td_fecha_fin'>{$fecha_fin}</td>
                    <td>{$vacaciones['estado']}</td>
                    <td>
                        {$btn_aprobar}
                        {$btn_rechazar}        
                    </td>
                </tr>";
    }

    $tabla .= '</tbody></table>';
    echo $tabla;

} else if ($opc == 'guardar_vacaciones'){
    $id = isset($_POST['id']) ? filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT, FILTER_NULL_ON_FAILURE) : 0;
    $estado = isset($_POST['estado']) ? filter_var($_POST['estado'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : '';
    $razon = isset($_POST['razon']) ? filter_var($_POST['razon'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : '';
    $ano_vacaciones = isset($_POST['ano_vacaciones']) ? (int) filter_var($_POST['ano_vacaciones'], FILTER_SANITIZE_NUMBER_INT) : '';

    //Obtener vacaciones desde BD para obtener sus datos
    $vacaciones_datos = vacaciones::get_vacaciones_by_id($id);
    if(count($vacaciones_datos) == 1){
        //Si existe
        $empleado_pernr_solicita = $vacaciones_datos[0]['empleado_pernr'];
        $empleado_sociedad = $vacaciones_datos[0]['empleado_sociedad'];
        $empleado_concepto_vac = $vacaciones_datos[0]['concepto_sap_id'];
        $fecha_inicio = $vacaciones_datos[0]['fecha_inicio_f'];//Fecha inicio formateada a ISO YYYY-MM-DD
        $fecha_fin = $vacaciones_datos[0]['fecha_fin_f']; //Fecha fin formateada a ISO YYYY-MM-DD
        $hora_inicio = ''; //Este no se pasa en vacaciones
        $hora_fin = ''; //Este no se pasa en vacaciones
    } else {
        echo utilities::swal_error('Error', 'No existen esas vacaciones.');
        return;
    }
    //El estado cuando es 1 es aprobada
    //El estado cuando es 2 es reprobada
    if($estado == 1){
        //Aprobada
        //En sap, es necesario el concepto, por ende hay que buscarlo
        //Para obtenerlo hay que buscar el  concepto id de la sociedad a la cual pertenece el empleado
        //  Ya lo estoy haciendo en $empleado_concepto_vac
        //La hora inicio y hora fin hay que pasarlo vacio


        //Retorna un simplexmlelement
        $respuesta = utilities::request_xml(soap_queries::save_vacaciones($fecha_inicio, $fecha_fin, $hora_inicio, $hora_fin, $empleado_concepto_vac, $empleado_pernr_solicita));
        $respuesta = utilities::make_xml_as_json($respuesta, '//ExCode|//ExMessage');
        //Buscar respuesta

        //$respuesta devuelve 2 array, con 1 array dentro
        $ExCode = (int) $respuesta[0][0]; //La primera es ExCode
        $ExMessage = $respuesta[1][0];//La segunda es MsgCode
        //Si fue ex == 0 es correcto
        //Si fue ex == 1 es incorrecto
        if($respuesta == 0){
            //EXitosa
            vacaciones::update_vacaciones_estado('aprobada', $razon, $id, $ano_vacaciones, $_SESSION['empleado_pernr'], null, 1);
            utilities::swal_success('¡Excelente!', 'Vacaciones aprobadas de forma correcta.');
        } else {
            utilities::swal_error('¡Error!', 'No se pudo aprobar vacaciones.');
        }

    } else if ($estado == 2){
        //Reprobada

        //Actualizar BD
        vacaciones::update_vacaciones_estado('rechazada', $razon, $id, $ano_vacaciones, null, $_SESSION['empleado_pernr']);
        utilities::swal_success('¡Excelente!', 'Vacaciones rechazadas de forma correcta.');
    }
}
