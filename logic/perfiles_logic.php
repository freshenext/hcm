<?php

require_once __DIR__ . '/../data/perfiles.php';
require_once __DIR__ . '/utl.php';

$opc = utilities::check_post_opc();

if ($opc == 'get_perfiles') {
    $perfiles = perfiles::get_perfiles();

    $tabla = "<table class='table table-striped'><thead>
            <th>Descripción</th>
            <th>Activo</th>
            <th></th>
        </thead><tbody>";

    foreach($perfiles as $perfil){
        $acciones = utilities::get_btn_edit('perfil_edit') . utilities::get_btn_edit('perfil_roles', '', 'VER ROLES') . utilities::get_btn_delete('perfil_del');

        $tabla .= "<tr perfil-id='{$perfil['id']}'>
                        <td class='perfil_desc'>{$perfil['descripcion']}</td>
                        <td class='perfil_activo' data-activo='{$perfil['activo']}'>{$perfil['activo_desc']}</td>
                        <td>{$acciones}</td>    
                    </tr>";
    }
    $tabla .= "</tbody></table>";
    echo $tabla;
} else if ($opc == 'insert_perfil') {
    $descripcion = isset($_POST['descripcion']) ? filter_var($_POST['descripcion'], FILTER_SANITIZE_STRING) : '';
    $activo = isset($_POST['activo']) ? (int) filter_var($_POST['activo'], FILTER_SANITIZE_NUMBER_INT) : '';
    $id = isset($_POST['id']) ? (int) filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT) : '';

    if($id == 0){
        //Insert
        perfiles::insert_perfil($descripcion,$activo);
    } else {
        //Update
        perfiles::update_perfil($id, $descripcion, $activo);
    }
    echo utilities::swal_success('', 'Perfil guardado de forma correcta.');
    echo "<script>
            get_perfiles();
        </script>";
} else if ($opc == 'delete_perfil'){
    $id = isset($_POST['id']) ? (int) filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT) : '';
    perfiles::delete_perfil($id);
    echo utilities::swal_success('', 'Perfil borrado de forma correcta.');
    echo "<script>
            get_perfiles();
        </script>";
} else if ($opc == 'get_perfil_rol'){
    $id = isset($_POST['id']) ? (int) filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT) : '';

    $roles = perfiles::get_perfil_rol($id);

    $tabla = "<table class='table table-striped'><thead>
                <th>Rol Descripción</th>
                <th>Activo</th>
                </thead><tbody>";
    foreach($roles as $rol){
        $tabla .= "<tr data-id='{$rol['id']}'>
                        <td>{$rol['descripcion']}</td>
                        <td>{$rol['activo']}</td>
                    </tr>";
    }
    $tabla .= "</tbody></table>";
    echo $tabla;

} else if ($opc == 'get_roles'){

    $roles = perfiles::get_roles();

    $option = utilities::build_select($roles, 'id', 'descripcion');

    echo $option;
}
