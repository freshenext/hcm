function send_request(url, data, comp = '', callback = '', poner_spinner_cargar = true, cambiar_comp = false, set_comp_data = true){
    /* Validar que sea form data */
    var processData = null;
    var contentType = null;

    if(data.tagName == 'FORM'){
        /* Es un form */
        /* Validar que este form tenga un attributo OPC */
        var opc = data.getAttribute('opc');
        var target = data.getAttribute('target');
        var id = data.getAttribute('data-id');
        data = new FormData(data);
        if(opc != null){
            /* Tiene, append al formdata*/
            // data = new FormData(data);
            data.append('opc', opc);
        }
        if(target != null){
            comp = target;
        }
        if(id != null){
            data.append('id', id);
        }
    }
    if(data instanceof FormData){
        /* True, entonces poner los processType y contentData false*/
        processData = false;
        contentType = false;
    }

    $.ajax({
            processData : processData == null ? this.processData : processData,
            contentType : contentType == null ? this.contentType : contentType,
            url: url,
            data: data,
            type: "POST",
            beforeSend: function () {
                //Poner spinner a cargar
                if(poner_spinner_cargar){
                    /* Si el comp es select, entonces agregar como opcion */
                    if($(comp).prop('tagName') == 'SELECT'){
                        $(comp).html('<option value="0">Cargando... </option>');
                    } else {
                        $(comp).html('<div class="text-center"><div></div>Cargando...<div><i class="fa fa-3x fa-cog fa-spin"></div></div>');
                    }

                }
            },
            success: function (data) {
                if(poner_spinner_cargar){
                    $(comp).html();
                }
                if(set_comp_data){
                    $(comp).html(data);
                }
                if(typeof(callback) == 'function'){
                    callback();
                }
                if(cambiar_comp){
                    $(comp).change();
                }

            },
            complete: function () {

            }
        }
    );
}

function send_success_msg(mensaje){
    return `<div class='text-center'>
        <button style='font-weight: bold;' class='btn btn-success'>
        ${mensaje}
        </button>
        </div>`;
}

function send_danger_msg(mensaje){
    return `<div class='text-center'>
        <button style='font-weight: bold;' class='btn btn-danger'>
        ${mensaje}
        </button>
        </div>`;
}

function send_info_msg(mensaje){
    return `<div class='text-center'>
        <button style='font-weight: bold;' class='btn btn-info'>
        ${mensaje}
        </button>
        </div>`;
}

function send_warning_msg(mensaje){
    return `<div class='text-center'>
        <button style='font-weight: bold;' class='btn btn-warning'>
        ${mensaje}
        </button>
        </div>`;
}

function limpiar_componentes(elementos){
    /* jQuery para limpiar cualquier componente */
    /* Se debe enviar un array de jQuery de elementos, EJ $('.btn') */
    elementos.filter('[type=text],[type=password],[type=email],[type=number], .span-id').val('');
    elementos.filter('select').val(0);
    elementos.filter('[type=checkbox]').prop('checked', false);
}

function crear_obj_desde_input_elementos(elementos){

    /*
     La idea está en que se pueda usar esta funcion en cualquier pagina para crear las variables para los metodos
     Hacerle output a traves del console log y copy paste
     1-Esto se divida en tres partes:
     2-Creacion de constantes
     3-Adicion de componente error
     4-Creacion de OBJ var
     5-Creacion de send_request
      */
    var variables = '';
    var validaciones = '';
    var obj_variables = [];

    var componente_error = elementos.find('.msg-error').attr('id');

    /* SI el elemento pasado es un div, buscar dentro de ese div los elementos */
    if(elementos.length == 1 && elementos.prop('tagName').toLowerCase() == 'div'){
        var elementos_temp = elementos;

        /* buscar el .span-id */
        elementos = elementos_temp.find('.span-id');

        /* buscar inputs, selects */
        elementos = elementos.add(elementos_temp.find('input, select'));

    }


    elementos.each(function(){
        var name = $(this).attr('name');
        var id_key = $(this).attr('id');
        var tipo_elemento = $(this).prop('tagName').toLowerCase();
        var valor = '';
        var tipo_input = $(this).attr('type');

        /* Validar si es span o input */
        switch(tipo_elemento) {
            case 'span' :
                variables += `const ${id_key} = $('#${id_key}').text();`;
                obj_variables.push(`${id_key} : ${id_key}`);
                break;
            case 'input'  :
                /* Detectar tipo de input */
                var elemento_selector = ''; /* variable para saber el selector*/
                var elemento_const_definicion = ''; /* variable para saber el nombre de la variable */
                if(name != null){
                    /* si name NO esta null entonces usar la nomenclatura name*/
                    elemento_selector = `'[name=${name}]'`;
                    elemento_const_definicion = name;
                } else {
                    /* de lo contrario usar ID */
                    elemento_selector = `'#${id_key}'`;
                    elemento_const_definicion = id_key;
                }

                switch (tipo_input) {
                    case 'text' : case 'password' : case 'email' : case 'number' :
                        variables += `const ${elemento_const_definicion} = $(${elemento_selector}).val();`;
                        validaciones += `if(${elemento_const_definicion} == ''){
                            componente_error.html(send_danger_msg(''));
                            return;
                        }\n`;
                        obj_variables.push(`${elemento_const_definicion} : ${elemento_const_definicion}`);
                        break;
                    case 'checkbox' :
                        variables += `const ${elemento_const_definicion} = $(${elemento_selector}).prop('checked') ? 1 : 0;`;
                        obj_variables.push(`${elemento_const_definicion} : ${elemento_const_definicion}`);
                        break;
                }
                /* agregar validaciones */

                break;
            case 'select' :
                /* para el select */
                var elemento_selector = ''; /* variable para saber el selector*/
                var elemento_const_definicion = ''; /* variable para saber el nombre de la variable */
                if(name != null){
                    /* si name NO esta null entonces usar la nomenclatura name*/
                    elemento_selector = `'[name=${name}] option:selected'`;
                    elemento_const_definicion = name;
                } else {
                    /* de lo contrario usar ID */
                    elemento_selector = `'#${id_key} option:selected'`;
                    elemento_const_definicion = id_key;
                }

                variables += `const ${elemento_const_definicion} = $(${elemento_selector}).val();`;
                validaciones += `if(${elemento_const_definicion} == 0){
                            componente_error.html(send_danger_msg(''));
                            return;
                        }\n`;
                obj_variables.push(`${elemento_const_definicion} : ${elemento_const_definicion}`);

        }
        variables += `\n`;
    });
    /* Creacion de validaciones */
    /* Primero, el componente error, normalmente tiene un id que contiene string _msg_ */

    variables += `\n`;
    variables += `var componente_error = $('#${componente_error}').html('');\n\n`;
    variables += validaciones;

    /* Creacion de obj */
    variables += `\n\n`;

    var string_keys_objeto = '';
    var longitud = obj_variables.length - 1;
    $.each(obj_variables, function(indice, valor){
        if(indice == longitud){
            //Al ultimo no se le agrega coma
            string_keys_objeto += valor;
        } else {
            string_keys_objeto += valor + ',\n';
        }
    })

    variables += `const obj = {
        opc : '',
        ${string_keys_objeto}
    }
    
    send_request('', obj, componente_error);`;
    swal.fire(variables);
    console.log(variables);
}