<?php

require_once __DIR__ . '/utl.php';
require_once __DIR__ . '/../data/roles.php';
require_once __DIR__ . '/../data/opciones.php';

$opc = utilities::check_post_opc();

if($opc == 'get_roles'){
    $roles = roles::get_roles();
    $tabla = "<table class='table table-striped'><thead>
                    <th>Descripción</th>
                    <th>Es Recursos Humanos</th>
                    <th>Activo</th>
                    <th></th>
                </thead><tbody>";
    $btn_opciones = "<button class='btn btn-primary' name='btn_ver_opciones'>Ver opciones <i class='fa fa-search'></i></button>";
    $btn_edit = utilities::get_btn_edit('edit_rol');
    foreach($roles as $rol){
        $tabla .= "<tr data-id='{$rol['id']}' data-es-rrhh='{$rol['es_rrhh']}'>
                        <td id='td_rol_desc'>{$rol['descripcion']}</td>
                        <td id='es_rrhh_desc'>{$rol['es_rrhh_desc']}</td>
                        <td id='td_rol_activo' data-activo='{$rol['activo']}' >{$rol['desc_activo']}</td>
                        <td>{$btn_opciones} {$btn_edit}</td>
                    </tr>";
    }
    $tabla .= "</tbody></table>";
    echo $tabla;
} else if ($opc == 'save_rol'){
    $id = isset($_POST['id']) ? (int) filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT) : '';
    $desc = isset($_POST['desc']) ? filter_var($_POST['desc'], FILTER_SANITIZE_STRING) : '';
    $activo = isset($_POST['activo']) ? (int) filter_var($_POST['activo'], FILTER_SANITIZE_NUMBER_INT) : 0;
    $es_rrhh = isset($_POST['es_rrhh']) ? (int) filter_var($_POST['es_rrhh'], FILTER_SANITIZE_NUMBER_INT) : 0;

    if($id == 0){
        /* Insert */
        roles::insert_rol($desc, $activo, $es_rrhh);
    } else {
        /* Update */
        roles::update_rol($desc, $activo, $id, $es_rrhh);
    }

    echo utilities::swal_success('', 'Rol guardado de forma exitosa.');
    echo "<script>
            $('#modal_roles').modal('hide');
            buscar_roles();
            </script>";
} else if ($opc == 'get_opc_rol'){
    /* Busca las opciones de un rol tipo tabla */
    $rol_id = isset($_POST['rol_id']) ? (int) filter_var($_POST['rol_id'], FILTER_SANITIZE_NUMBER_INT) : '';

    $opciones = roles::get_roles_opc($rol_id);

    $tabla = "<table class='table table-striped'>
                <thead>
                    <th>Opción</th>
                    <th></th>
                </thead><tbody>";

    $btn_borrar = "<button type='button' class='btn btn-danger' id='borrar_opc_rol'>BORRAR <i class='fa fa-remove'></i></button>";
    foreach($opciones as $opcion){
        $tabla .= "<tr data-id='{$opcion['id']}'>
                        <td>{$opcion['opc_desc']}</td>
                        <td style='text-align: center;'>{$btn_borrar}</td>
                    </tr>";
    }
    $tabla .= "</tbody></table>";
    echo $tabla;
} else if ($opc == 'get_sel_opciones'){
    $opciones = roles::get_opciones_categoria();

    echo utilities::build_select($opciones, 'id', 'descripcion');
} else if ($opc == 'agregar_rol_opciones'){
    $rol_id = isset($_POST['rol_id']) ? (int) filter_var($_POST['rol_id'], FILTER_SANITIZE_NUMBER_INT) : '';
    $opcion_id = isset($_POST['opcion_id']) ? (int) filter_var($_POST['opcion_id'], FILTER_SANITIZE_NUMBER_INT) : '';

    roles::save_roles_opc($rol_id, $opcion_id);
    echo utilities::swal_success('', 'Opción agregada al rol.');
} else if ($opc == 'borrar_rol_opc'){
    $id = isset($_POST['id']) ? (int) filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT) : '';
    roles::delete_rol_opc($id);
    echo utilities::swal_success('', 'Rol borrado de forma exitosa.');
}
