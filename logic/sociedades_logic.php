<?php
require_once __DIR__ . '/utl.php';
require_once __DIR__ . '/../data/sociedades.php';
require_once __DIR__ .'/../query/soap_requests.php';
$opc = utilities::check_post_opc();

if($opc == 'get_sociedades'){
    $tbl_sociedades = sociedades::get_sociedades();

    if(count($tbl_sociedades) > 0){
        //Existen sociedades
        echo utilities::send_info_msg('Ya existen sociedades cargadas.');
        echo "<script>get_tbl_sociedades();</script>";
    } else {
        //Cargarlas
        $xml = utilities::request_xml(soap_queries::get_sociedades());
        $xml = utilities::make_xml_as_json($xml, '//item');
        foreach($xml as $indice => $sociedad){
            $bukrs = $sociedad['Bukrs']; //codigo sociedad
            $butxt = $sociedad['Butxt']; //nombre sociedad
            sociedades::insert_sociedad($bukrs, $butxt);
        }
        echo 1;
    }

} else if ($opc == 'get_tbl_sociedades'){

    $tbl_sociedades = sociedades::get_sociedades();

    if(count($tbl_sociedades) > 0){
        //Existen sociedades
        $table = "<table class='table table-striped'>
                    <thead>
                        <th>Nombre Sociedad</th>
                        <th>Codigo</th>
                        <th>Concepto Vacaciones</th>
                        <th>Lgart (Si tiene)</th>
                        <th></th>
                    </thead>
                    <tbody>";
        foreach($tbl_sociedades as $sociedad){
            $asignar_concepto = utilities::get_btn_edit('asignar_concepto', '', 'Asignar Concepto');
            $table .= "<tr data-id='{$sociedad['id']}'>
                            <td>{$sociedad['butxt']}</td>
                            <td>{$sociedad['bukrs']}</td>
                            <td>{$sociedad['concepto_desc']}</td>
                            <td>{$sociedad['lgart']}</td>
                            <td>{$asignar_concepto}</td>
                        </tr>";
        }
        $table .= "</tbody></table>";
        echo $table;
    } else {
        //Actualmente no existen sociedades
        echo utilities::send_info_msg('Actualmente no existen sociedades cargadas.');
    }
} else if ($opc == 'update_sociedad_concepto'){
    $id_sociedad = isset($_POST['id']) ? (int) filter_var($_POST['id'], FILTER_SANITIZE_NUMBER_INT) : '';
    $id_concepto = isset($_POST['concepto_id']) ? (int) filter_var($_POST['concepto_id'], FILTER_SANITIZE_NUMBER_INT) : '';

    sociedades::update_sociedad_concepto($id_sociedad, $id_concepto);
    echo utilities::swal_success('¡Exito!', 'Concepto guardado correctamente.');
    echo "<script>get_tbl_sociedades();</script>";
}