<?php require 'inc/_global/config.php'; ?>
<?php require 'inc/backend/config.php'; ?>


<?php
// Codebase - Page specific configuration

$cb->l_m_content        = 'narrow';
$cb->l_header_fixed     = true;
$cb->l_header_style     = '';
$cb->l_sidebar_inverse  = true;


$cb->title = 'HCM | Indice';


?>
<?php require 'inc/_global/views/head_start.php'; ?>
<?php require 'inc/_global/views/head_end.php'; ?>
<?php require 'inc/_global/views/page_start.php'; ?>

<!-- Page Content -->
<div class="content">
    <div class="row">
        <div class="col-12">
 <?php
$files = scandir('./');
foreach($files as $file){
    echo "<a href='http://md.localhost/${file}'>{$file}</a><br>";
}
?>
        </div>
    </div>
</div>
<!-- END Page Content -->

<?php require 'inc/_global/views/page_end.php'; ?>
<?php require 'inc/_global/views/footer_start.php'; ?>

<!-- Page JS Plugins -->
<?php $cb->get_js('js/plugins/chartjs/Chart.bundle.min.js'); ?>

<!-- Page JS Code -->
<?php $cb->get_js('js/pages/be_pages_dashboard.min.js'); ?>

<?php require 'inc/_global/views/footer_end.php'; ?>