/*
 *  Document   : op_auth_signin.js
 *  Author     : pixelcave
 *  Description: Custom JS code used in Sign In Page
 */

// Form Validation, for more examples you can check out https://github.com/jzaefferer/jquery-validation
class OpAuthSignIn {
    /*
     * Init Sign In Form Validation
     *
     */
    static initValidationSignIn() {
        jQuery('.js-validation-signin').validate({
            errorClass: 'invalid-feedback animated fadeInDown',
            errorElement: 'div',
            errorPlacement: (error, e) => {
                jQuery(e).parents('.form-group > div').append(error);
            },
            highlight: e => {
                jQuery(e).closest('.form-group').removeClass('is-invalid').addClass('is-invalid');
            },
            success: e => {
                jQuery(e).closest('.form-group').removeClass('is-invalid');
                jQuery(e).remove();
            },
            rules: {
                'login-username': {
                    required: true,
                    minlength: 3
                },
                'login-password': {
                    required: true,
                    minlength: 6
                },
                'login-email' : {
                    required : true
                }
            },
            messages: {
                'login-username': {
                    required: 'Por favor ingrese su nombre de usuario',
                    minlength: 'Your username must consist of at least 3 characters'
                },
                'login-password': {
                    required: 'Por favor inserte su contraseña',
                    minlength: 'Su contraseña debe de ser mínimo de 6 letras.'
                },
                'login-email' : {
                    required : 'Por favor ingrese un correo electrónico válido'
                }
            }
        });
    }

    /*
     * Init functionality
     *
     */
    static init() {
        this.initValidationSignIn();
    }
}

// Initialize when page loads
jQuery(() => { OpAuthSignIn.init(); });
