<?php

Class procesos_query {

    public static function getProcesos(){
        return "select * from procesos";
    }
    public static function insertProceso(){
        return "insert into procesos (nombre, descripcion, fecha_inicio, fecha_fin)
                VALUES (:nombre, :descripcion, :fecha_inicio, :fecha_fin);";
    }
}