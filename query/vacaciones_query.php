<?php

Class vacaciones_query {

    public static function get_vacaciones_by_id(){
        return "select vac.*,
                ifnull(emp.bukrs, '') as empleado_sociedad,
                ifnull(soc.concepto_ausentismo_id, 0) as concepto_id,
                ifnull(concep.subty, '') as concepto_sap_id,
                date(fecha_inicio) as fecha_inicio_f,
                date(fecha_fin) as fecha_fin_f
                from vacaciones vac
                    left join empleados emp on emp.pernr = vac.empleado_pernr
                        left join sociedades soc on soc.bukrs = emp.bukrs
                            left join conceptos concep on concep.id = soc.concepto_ausentismo_id 
                where vac.id = :id;";
    }
    public static function get_vacaciones_activas(){
        return "
select vac.*, emp_solicita.ename, emp_solicita.orgehtext as organizacion from vacaciones vac
                    left join empleados emp_solicita on emp_solicita.pernr = vac.empleado_pernr
                    left join empleados emp_aprueba on emp_aprueba.pernr = vac.empleado_pernr_aprueba
                    left join empleados emp_reprueba on emp_reprueba.pernr = vac.empleado_pernr_reprueba
                    where (empleado_pernr regexp :empleado or :es_rrhh = 1)
                    and (vac.estado = 'pendiente')
                    and (vac.fecha_solicitada = :fecha_solicitud_creada or :fecha_solicitud_creada = '')
                    and case
                        when :fecha_inicio = '' AND :fecha_fin = '' then true
                        when :fecha_inicio != '' then (date(fecha_inicio) >= date(:fecha_inicio) )
                        when :fecha_fin != '' then (date(fecha_fin) <= date(:fecha_fin))
                        else true
                    end;
";
    }

    public static function insert_vacaciones(){
        return "insert into vacaciones (empleado_pernr, fecha_inicio, fecha_fin, comentario, dias)
                    VALUES(
                           :empleado_pernr, :fecha_inicio, :fecha_fin, :comentario, :dias
                          )
                ";
    }

    public static function update_vacaciones_estado(){
        return "update vacaciones
                set estado = :estado,
                razon_estado = :razon,
                ano = :ano,
                empleado_pernr_aprueba = :empleado_pernr_aprueba,
                empleado_pernr_reprueba = :empleado_pernr_reprueba,
                registrada_en_sap = :registrada_en_sap
                where id = :id;";
    }
}
