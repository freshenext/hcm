<?php

Class roles_query {
    public static function get_roles(){
        return "select *, case when activo = 1 then 'Si' else 'No' end as desc_activo,
                case when es_rrhh = 1 then 'Si' else 'No' end as es_rrhh_desc
                from roles";
    }

    public static function insert_rol(){
        return "insert into roles (descripcion, activo, es_rrhh) VALUES (:desc, :activo, :es_rrhh);";
    }

    public static function update_rol(){
        return "update roles
        set descripcion = :desc,
        activo = :activo,
        es_rrhh = :es_rrhh
        where id = :id";
    }

    public static function get_roles_opc(){
        return "
        select rco.id as id, o.descripcion as opc_desc, rol.descripcion as rol_desc
         from roles_categoria_opciones rco
            join categoria_opciones co on rco.categoria_opciones_id = co.id
                join opciones o on co.opcion_id = o.id
                join roles rol on rol.id = rco.rol_id
        where rco.rol_id = :rol_id";
    }

    public static function get_opciones_categoria(){
        return "
        select concat(c.descripcion, ' => ', opc.descripcion) as 'descripcion', co.id as 'id'
        from categoria_opciones co
                 join opciones opc on opc.id = co.opcion_id
                 join categorias c on co.categoria_id = c.id;";
    }

    public static function save_roles_opc(){
        return "insert into roles_opciones (rol_id, opcion_id) VALUES (:rol_id, :opcion_id);";
    }

    public static function delete_rol_opc(){
        return "delete from roles_opciones where id = :id";
    }
}
