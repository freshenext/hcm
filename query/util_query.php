<?php

Class util_query {

    public static function get_columnas_activas(){
        return "select nombre_columna from columnas where activo = 1;";
    }

    public static function set_columna_estatus(){
        return "update columnas set activo = :activo where id = :id";
    }
}