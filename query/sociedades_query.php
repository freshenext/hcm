<?php

Class sociedades_query {
    public static function get_sociedades(){
        return "select soc.*, conp.descripcion as concepto_desc from sociedades soc
                    left join conceptos conp on conp.id = soc.concepto_ausentismo_id;";
    }

    public static function insert_sociedad(){
        return "insert into sociedades (bukrs, butxt)
        VALUES (:bukrs,  :butxt)";
    }

    public static function update_sociedad_concepto(){
        return "update sociedades
        set concepto_ausentismo_id = :concepto_id
        where id = :id";
    }
}
