<?php

Class categorias_query  {

    public static function get_categorias(){
        return "select *, case when activo = 1 then 'Si' else 'No' end as activada from categorias;";
    }

    public static function insert_categoria(){
        return "insert into categorias (descripcion, orden, activo, icono) VALUES (:nombre, :orden, :activo, :icono)";
    }

    public static function update_categoria(){
        return "update categorias
        set descripcion = :nombre, activo = :activo, orden = :orden, icono = :icono
        where id = :id";
    }

    public static function get_opciones_categoria(){
        return "select *, cop.id as id_categoria_opcion
                from categoria_opciones cop
                    join opciones opc on opc.id = cop.opcion_id 
                where categoria_id = :categoria_id";
    }

    public static function insert_opcion_categoria(){
        return "insert into categoria_opciones (opcion_id, categoria_id, orden) VALUES (:opcion_id, :categoria_id, :orden);";
    }

    public static function update_opcion_categoria(){
        return "update categoria_opciones
                set orden = :orden
                where id = :id";
    }

    public static function delete_opcion_categoria(){
        return "delete from categoria_opciones where id = :id";
    }

    public static function delete_categoria(){
        return "delete from categorias where id = :id;";
    }

    public static function get_iconos(){
        return "select * from iconos;";
    }
}