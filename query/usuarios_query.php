<?php

Class usuarios_query {
    public static function get_usuarios(){
        return "select * from empleados";
    }

    public static function get_empleado_roles(){
        return "select empr.*, rr.descripcion as rol_desc from empleado_roles empr
                    join roles rr on rr.id = empr.rol_id
                    where empr.empleado_id = :empleado_id";
    }

    public static function insert_empleado_rol(){
        return "insert into empleado_roles (rol_id, empleado_id) VALUES (:rol_id, :empleado_id)";
    }

    public static function delete_empleado_rol(){
        return "delete from empleado_roles where id= :id";
    }

    public static function get_empleado_opciones(){
        return "select opc.*, catopc.categoria_id, catg.descripcion as categoria_desc
        from empleado_roles empr
            join roles_categoria_opciones rco on rco.rol_id = empr.rol_id
                join categoria_opciones catopc on catopc.id = rco.categoria_opciones_id 
                    join opciones opc on opc.id = catopc.opcion_id
                    join categorias catg on catg.id = catopc.categoria_id
        where empleado_id = :empleado_id";
    }

    public static function get_empleado_categorias(){
        return "select ctg.*
        from empleado_roles empr
            join roles_categoria_opciones rco on rco.rol_id = empr.rol_id
                join categoria_opciones catopc on catopc.id = rco.categoria_opciones_id
                    join categorias ctg on ctg.id = catopc.categoria_id
        where empleado_id = :empleado_id
        group by ctg.id";
    }
}