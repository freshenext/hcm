<?php


Class soap_queries{

    public static function get_empleado($empleado_id = ''){
        return "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">\r\n   <soap:Header/>\r\n   <soap:Body>\r\n      <urn:Employee_masterdata>\r\n         <!--Optional:-->\r\n         <ExTData>\r\n         </ExTData>\r\n         <!--Optional:-->\r\n         <ImBukrs></ImBukrs>\r\n         <!--Optional:-->\r\n         <ImLangu></ImLangu>\r\n         <!--Optional:-->\r\n         <ImPernr>{$empleado_id}</ImPernr>\r\n         <!--Optional:-->\r\n         <ImSubtyAddress></ImSubtyAddress>\r\n         <!--Optional:-->\r\n         <ImSubtyDocid></ImSubtyDocid>\r\n         <!--Optional:-->\r\n         <ImSubtyDrive></ImSubtyDrive>\r\n         <!--Optional:-->\r\n         <ImSubtyPassport></ImSubtyPassport>\r\n         <!--Optional:-->\r\n         <ImSubtySsn></ImSubtySsn>\r\n      </urn:Employee_masterdata>\r\n   </soap:Body>\r\n</soap:Envelope>\r\n";
    }

    public static function get_dias_disfrute($fecha_inicio, $fecha_fin, $empleado_id = '01000001', $concepto_id = '0100'){
        return "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">
   <soap:Header/>
   <soap:Body>
      <urn:Employee_absences_simu>
         <ImBegda>{$fecha_inicio}</ImBegda>
         <ImEndda>{$fecha_fin}</ImEndda>
         <ImPernr>{$empleado_id}</ImPernr>
         <ImSubty>{$concepto_id}</ImSubty>
      </urn:Employee_absences_simu>
   </soap:Body>
</soap:Envelope>";
    }

    public static function get_conceptos(){
        return "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">
   <soap:Header/>
   <soap:Body>
      <urn:Payroll_wagetypes>
         <!--Optional:-->
         <ExTWagetypes>
            <!--Zero or more repetitions:-->

         </ExTWagetypes>
         <ImInfty>2001</ImInfty>
         <!--Optional:-->
         <ImLangu>S</ImLangu>
         <!--Optional:-->
         <ImLgart></ImLgart>
         <!--Optional:-->
         <ImMolga>99</ImMolga>
      </urn:Payroll_wagetypes>
   </soap:Body>
</soap:Envelope>";
    }

    public static function get_sub_empleados($imburks = 1000, $empleado_id = 1000001){
        return "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">
   <soap:Header/>
   <soap:Body>
      <urn:Employee_dependency>
         <!--Optional:-->
         <ExTEmployees>
            <!--Zero or more repetitions:-->
         </ExTEmployees>
         <ImBukrs>${imburks}</ImBukrs>
         <ImPernr>${empleado_id}</ImPernr>
      </urn:Employee_dependency>
   </soap:Body>
</soap:Envelope>";
    }

    public static function get_dias_ausentes($empleado_id = 1000001){
        return "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">
   <soap:Header/>
   <soap:Body>
      <urn:Employee_absences>
         <!--Optional:-->
         <ExTAbsences>
            <!--Zero or more repetitions:-->
 
         </ExTAbsences>
         <ImBegda>0000-00-00</ImBegda>
         <ImEndda>9999-99-99</ImEndda>
         <!--Optional:-->
         <ImPernr>${empleado_id}</ImPernr>
         <!--Optional:-->
         <ImSubty></ImSubty>
      </urn:Employee_absences>
   </soap:Body>
</soap:Envelope>";
    }

    public static function get_dias_disfrutados($empleado_id){
        return "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">
   <soap:Header/>
   <soap:Body>
      <urn:Employee_quotas>
         <!--Optional:-->
         <ExTQuotas>
         </ExTQuotas>
         <ImBegda>0000-00-00</ImBegda>
         <ImEndda>9999-99-99</ImEndda>
         <!--Optional:-->
         <ImPernr>${empleado_id}</ImPernr>
         <!--Optional:-->
         <ImSubty></ImSubty>
      </urn:Employee_quotas>
   </soap:Body>
</soap:Envelope>";
    }

    public static function save_vacaciones($fecha_inicio, $fecha_fin, $hora_inicio, $hora_fin, $concepto_id, $empleado_pernr){
        return "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">
   <soap:Header/>
   <soap:Body>
      <urn:Employee_absences_create>
         <!--Optional:-->
         <ImBegda>{$fecha_inicio}</ImBegda>
         <!--Optional:-->
         <ImBeguz>{$hora_inicio}</ImBeguz>
         <!--Optional:-->
         <ImEndda>{$fecha_fin}</ImEndda>
         <!--Optional:-->
         <ImEnduz>{$hora_fin}</ImEnduz>
         <ImPernr>{$empleado_pernr}</ImPernr>
         <ImSubty>{$concepto_id}</ImSubty>
      </urn:Employee_absences_create>
   </soap:Body>
</soap:Envelope>";
    }

    public static function get_sociedades(){
        return "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">
   <soap:Header/>
   <soap:Body>
      <urn:Financial_companies>
         <!--Optional:-->
         <ExTCompanies>
            <!--Zero or more repetitions:-->
            <item>
               
            </item>
         </ExTCompanies>
         <!--Optional:-->
         <ImLand1>DO</ImLand1>
      </urn:Financial_companies>
   </soap:Body>
</soap:Envelope>";
    }

    public static function get_org_chart($via_evaluacion, $fecha = ''){
        if($fecha == ''){
            /* Si fecha null, entonces utilizar fecha de hoy*/
            $fecha = date('Y-m-d');
        }
        return "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:urn=\"urn:sap-com:document:sap:soap:functions:mc-style\">
   <soap:Header/>
   <soap:Body>
      <urn:Organizational_full_chart>
         <!--Optional:-->
         <ExTChart>
            <!--Zero or more repetitions:-->

         </ExTChart>
         <!--Optional:-->
         <ImBegda>{$fecha}</ImBegda>
         <!--Optional:-->
         <ImEndda>{$fecha}</ImEndda>
         <!--Optional:-->
         <ImObjid></ImObjid>
         <!--Optional:-->
         <ImOtype>O</ImOtype>
         <!--Optional:-->
         <ImPlvar>01</ImPlvar>
         <!--Optional:-->
         <ImWegid>{$via_evaluacion}</ImWegid>
      </urn:Organizational_full_chart>
   </soap:Body>
</soap:Envelope>";
    }

}