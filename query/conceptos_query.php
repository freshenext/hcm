<?php

Class conceptos_query {

    public static function get_conceptos(){
        return "select *, 
                case when activo = 1 then 'Si' else 'No' end as activo_desc,
                case when es_vacaciones = 1 then 'Si' else 'No' end as vacaciones_desc 
                from conceptos;";
    }

    public static function insert_concepto(){
        return "insert into conceptos (descripcion, mandt, sprsl,infty, subty)
        VALUES (
                :descripcion,
                :mandt,
                :sprsl,
                :infty,
                :subty
            )";
    }

    public static function update_concepto_vacacional(){
        return "update conceptos set es_vacaciones = 0;
                update conceptos set es_vacaciones = 1 where id = :id;";
    }
}
