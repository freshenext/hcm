<?php

Class perfiles_query {

    public static function get_perfiles(){
        return "select *, case when activo = 1 then 'Si' else 'No' end as activo_desc from perfiles";
    }

    public static function insert_perfil(){
        return "insert into perfiles (descripcion, activo) VALUES (:descripcion, :activo)";
    }

    public static function update_perfil(){
        return "update perfiles
        set descripcion = :descripcion,
        activo = :activo
        where id = :id";
    }

    public static function delete_perfil(){
        return "delete from perfiles where id = :id";
    }

    public static function get_perfil_rol(){
        return "select rpl.* from perfil_roles per
                    join roles rpl on rpl.id = per.rol_id
                where perfil_id = :perfil_id";
    }

    public static function get_roles(){
        return "select * from roles;";
    }
}