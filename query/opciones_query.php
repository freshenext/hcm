<?php

Class opciones_query {

    public static function get_opciones(){
        return "select *, 
                case when activo = 1 then 'Si' else 'No' end as activada, 
                concat(descripcion, ' - ', link) as descTodo
                from opciones;";
    }

    public static function insert_opcion(){
        return "insert into opciones (descripcion, link, activo)
        VALUES (
            :descripcion,
            :link,
            :activado
        )";
    }

    public static function update_opcion(){
        return "update opciones
        set descripcion = :descripcion,
        link = :link,
        activo = :activado
        where id = :id";
    }
}