<?php

Class parametros_query {
    public static function get_parametros(){
        return "select * from parametros;";
    }

    public static function insert_parametros(){
        return "insert into parametros (imagen_empresa, imagen_personal, imagen_puesto)
                VALUES (
                :imagen_empresa,
                :imagen_personal,
                :imagen_puesto
                )";
    }

    public static function validar_parametros_existe(){
        return "select count(*) as existe from parametros;";
    }

    public static function update_parametros(){
        return "update parametros
        set imagen_empresa = :imagen_empresa,
        imagen_personal = :imagen_personal,
        imagen_puesto = :imagen_puesto
        where id = :id";
    }
}
