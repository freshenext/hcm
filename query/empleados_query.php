<?php

Class empleados_query {

    public static function get_empleados(){
        return "select * from empleados where email = :email and password = SHA2(:pass,256);";
    }

    public static function get_empleados_modo_prueba(){
        return "select *,
                case when (select count(*) from empleado_roles empr join roles rolt on rolt.id = empr.rol_id
                where empr.empleado_id = emp.id and rolt.es_rrhh = 1) > 0 then 1 
                else 0 
                end as es_rrhh from empleados emp where emp.email = :email;";
    }

    public static function get_all_empleados(){
        return "select * from empleados;";
    }

    public static function insert_empleados(){
        return "insert into empleados (bukrs, butxt, pernr, entrydate, leavingdate, ename, molga, formofaddress, firstname, lastname, nameatbirth, secondname, middlename, fullname, knownas, academicgrade, secondacadgrade, aristrocratictitle, surnameprefix, secondnameprefix, initials, gender, dateofbirth, birthplace, stateofbirth, countryofbirth, maritalstatus, maritalstatussince, numberofchildren, religion, language, languageiso, nationality, secondnationality, thirdnationality, idnumber, nameofformofaddress, nameofgender, nameofstateofbirth, werks, werkstext, btrtl, btrtltext, persg, persgtext, persk, persktext, abkrs, abkrstext, ansvh, ansvhtext, kokrs, kostl, kostltext, orgeh, orgehtext, stell, stelltext, plans, planstext, cpind, trfar, trfartext, trfgb, trfgbtext, trfgr, trfst, bsgrd, email, username, betrg, anzhl, docid, passport, ssn, drive, addresstype, coname, streetandhouseno, scndaddressline, city, district, postalcodecity, state, country, telephonenumber, nameofaddresstype, nameofstate, nameofcountry, datum, motpr, tprog, tagty, ftkla, varia, tpkla, zmodn, stdaz, activ, nat01, nat02, pamod, paymentmethod, payee, bankcountry, bankkey, accountno, nameofbanktype, nameofpaymentmethod, nameofcurrency, ctedt, stat2)
                              VALUES (:bukrs, :butxt, :pernr, :entrydate, :leavingdate, :ename, :molga, :formofaddress, :firstname, :lastname, :nameatbirth, :secondname, :middlename, :fullname, :knownas, :academicgrade, :secondacadgrade, :aristrocratictitle, :surnameprefix, :secondnameprefix, :initials, :gender, :dateofbirth, :birthplace, :stateofbirth, :countryofbirth, :maritalstatus, :maritalstatussince, :numberofchildren, :religion, :language, :languageiso, :nationality, :secondnationality, :thirdnationality, :idnumber, :nameofformofaddress, :nameofgender, :nameofstateofbirth, :werks, :werkstext, :btrtl, :btrtltext, :persg, :persgtext, :persk, :persktext, :abkrs, :abkrstext, :ansvh, :ansvhtext, :kokrs, :kostl, :kostltext, :orgeh, :orgehtext, :stell, :stelltext, :plans, :planstext, :cpind, :trfar, :trfartext, :trfgb, :trfgbtext, :trfgr, :trfst, :bsgrd, :email, :username, :betrg, :anzhl, :docid, :passport, :ssn, :drive, :addresstype, :coname, :streetandhouseno, :scndaddressline, :city, :district, :postalcodecity, :state, :country, :telephonenumber, :nameofaddresstype, :nameofstate, :nameofcountry, :datum, :motpr, :tprog, :tagty, :ftkla, :varia, :tpkla, :zmodn, :stdaz, :activ, :nat01, :nat02, :pamod, :paymentmethod, :payee, :bankcountry, :bankkey, :accountno, :nameofbanktype, :nameofpaymentmethod, :nameofcurrency, :ctedt, :stat2);";
    }
}