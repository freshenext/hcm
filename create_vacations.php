<?php
require_once __DIR__ . '/logic/utl.php';
$cb =  utilities::load_template()
?>
<!-- Page Content -->
<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="block block-themed">
                <div class="block-header">
                    <h3 class="block-title">Solicitud de Vacaciones</h3>
                </div>
                <div class="block-content">
                    <div class="row form-group">
                        <div class="col-4 form-group">
                            <div class="form-material">
                                <input type="text" class="js-flatpickr form-control js-flatpickr-enabled flatpickr-input" id="fecha_inicio" name="fecha_inicio" placeholder="d-m-Y" data-allow-input="false">
                                <label for="fecha_inicio">Fecha Inicio</label>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-4 form-group">
                            <div class="form-material">
                                <input type="text" class="js-flatpickr form-control js-flatpickr-enabled flatpickr-input" id="fecha_fin" name="fecha_fin" placeholder="d-m-Y" data-allow-input="false">
                                <label for="fecha_inicio">Fecha Fin</label>
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12 form-group">
                            <label for="comentario">Comentario: </label>
                            <textarea class="form-control" id="comentario" name="comentario" rows="5"></textarea>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12 text-center form-group">
                            <button type="button" class="btn btn-rounded btn-success" id="solicitar_vacaciones">Solicitar Vacaciones</button>
                        </div>
                    </div>
                    <div class="row form-group">
                        <!-- SPINNER -->
                        <div class="col-12 text-center" id="msgVacaciones">

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="col-12">
            <div class="block block-themed">
                <div class="block-header">
                    <h3 class="block-title">Vacaciones Solicitadas</h3>
                </div>
                <div class="block-content">
                    <div class="row form-group">
                        <div class="col-12 form-group" id="tbl_vacaciones_solicitadas">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="block block-themed">
                <div class="block-header">
                    <h3 class="block-title">Plan de Vacaciones</h3>
                </div>
                <div class="block-content">
                    <div class="row form-group">
                        <div class="col-12 form-group" id="tbl_historico_vacaciones">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- END Page Content -->


<script>
    //Cuando cargue el documento
    $(document).ready(function () {
        get_historico_vacaciones();
    });

    function save_vacation(){
        var obj = {
            opc : 'guardar_vacaciones'
        }
        send_request('/logic/empleados_logic.php', obj, '#msgVacaciones');
    }

    function get_historico_vacaciones(){
        var obj = {
            opc : 'get_historico_vacaciones'
        }
        send_request('/logic/empleados_logic.php',obj, '#tbl_historico_vacaciones');
    }


    //Iniciar elementos date
    $('#fecha_inicio, #fecha_fin').flatpickr();

    //Al dar click al boton de vacaciones
    $('#solicitar_vacaciones').click(function () {
        var fecha_inicio = $('#fecha_inicio').val();
        var fecha_fin = $('#fecha_fin').val();
        var comentario = $('#comentario').val();
        //validar

        //Validaciones fecha inicio, fecha fin, y timestamp unix
        if(fecha_inicio == ''){
            Swal.fire({
                title : '¡Atención!',
                text : 'No insertó fecha de inicio.',
                type : 'info'
            });
            return;
        }
        if(fecha_fin == ''){
            Swal.fire({
                title : '¡Atención!',
                text : 'No insertó fecha fin.',
                type : 'info'
            });
            return;
        }
        if(Date.parse(fecha_inicio) > Date.parse(fecha_fin)){
                        Swal.fire({
                title : '¡Atención!',
                text : 'La fecha inicial no puede ser mayor que la fecha final.',
                type : 'info'
            });
            return;

        }


        var obj = {
            opc : 'solicitar_vacaciones',
            fecha_inicio : fecha_inicio,
            fecha_fin : fecha_fin,
            comentario : comentario
        }
        send_request('/logic/empleados_logic.php', obj, '#msgVacaciones');
    });
</script>

<?php utilities::load_template_footer($cb); ?>


