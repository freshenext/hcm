<?php
/**
 * head_end.php
 *
 * Author: Francis
 *
 * (continue) The first block of code used in every page of the template
 *
 * The reason we separated head_start.php and head_end.php is for enabling
 * us to include between them extra plugin CSS files needed only in specific pages
 *
 */
?>

    <!-- Fonts and Codebase framework -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
    <link rel="stylesheet" id="css-main" href="<?php echo $cb->assets_folder; ?>/css/codebase.min.css">

    <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
    <link rel="stylesheet" href="/assets/js/plugins/sweetalert2/sweetalert2.css">
    <script src="/assets/js/plugins/sweetalert2/sweetalert2.js"></script>
    <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
    <?php if ($cb->theme) { ?>
    <link rel="stylesheet" id="css-theme" href="<?php echo $cb->assets_folder; ?>/css/themes/<?php echo $cb->theme; ?>.min.css">
    <?php } ?>

    <!-- FLAT PICKR -->
    <link rel="stylesheet" href="/assets/js/plugins/flatpickr/flatpickr.css">
    <!-- END Stylesheets -->

    <!-- INICIO DE CARGA DE SCRIPTS -->

    <!-- CodeBase Core -->
    <script src="/assets/js/codebase.core.min.js"></script>

    <!-- CodeBase App -->
    <script src="/assets/js/codebase.app.min.js"></script>
    <!-- Mi script -->
    <script src="/logic/utl.js"></script>
    <!-- Flatpicrk -->
    <script src="/assets/js/plugins/flatpickr/flatpickr.js"></script>

    <!-- ChartJS for the template -->
    <script src="/assets/js/plugins/chartjs/Chart.bundle.min.js"></script>

    <!-- Dashboard Custom JS -->
    <script src="/assets/js/pages/be_pages_dashboard.min.js"></script>

    <!-- Print This -->
    <script src="/assets/js/plugins/printthis/printThis.js"></script>

    <!-- HTML2C -->
<script src="/assets/js/plugins/html2canvas/html2canvas.min.js"></script>

</head>
<body>
