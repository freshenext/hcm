<?php
require_once __DIR__ . '/logic/utl.php';
$cb = utilities::load_template();
?>

<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="block block-themed">
                <div class="block-header">
                    <h3 class="block-title">Perfiles</h3>
                </div>
                <div class="block-content">
                    <div class="row">
                        <!-- FILTROS COMING SOON -->
                        <div class="col-12 text-center">
                            <button type="button" class="btn btn-primary" id="buscar_perfil">Buscar <i class="fa fa-search"></i></button>
                            <button type="button" class="btn btn-primary" id="agregar_perfil">Agregar <i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12 text-center">
                            <div id="msgPerfiles"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12" id="tbl_perfiles"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_perfiles" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popin">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Perfil<!-- TITULO --></h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <span hidden id="txt_id_perfil"></span>
                    <div class="row form-group">
                        <!-- CONTENIDO -->
                        <div class="col-12">
                            <label>Descripción: </label>
                            <input type="text" class="form-control" id="inp_desc">
                        </div>
                        <div class="col-12">
                            <label>Activo: </label>
                            <input type="checkbox" class="form-control" id="cbx_perfil" checked>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-12" id="msgPerfilesModal"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-alt-primary" id="guardar_perfil">
                    <i class="fa fa-check"></i> Guardar
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal_perfil_roles" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popin">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <span hidden id="txt_perfil_id"></span>
                    <h3 class="block-title">Roles del Perfil <span id="perfil_desc"></span><!-- TITULO --></h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">

                    <div class="row">
                        <!-- CONTENIDO -->
                        <div class="col-12 form-group">
                            <label>Rol: </label>
                            <select class="form-control" id="sel_roles"></select>
                        </div>
                        <div class="col-12 form-group text-center">
                            <button type="button" id="agregar_rol" class="btn btn-primary">Agregar Rol <i class="fa fa-plus"></i></button>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-12" id="msgRolesPerfil"></div>
                    </div>
                    <div class="row">
                        <div class="col-12" id="tbl_perfil_roles"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-alt-primary">
                    <i class="fa fa-check"></i> Guardar
                </button>
            </div>
        </div>
    </div>
</div>
<script>
    $('#buscar_perfil').click(function(){
        get_perfiles();
    });

    $('#agregar_perfil').click(function(){

        $('#modal_perfiles').modal();
    });

    $('#guardar_perfil').click(function(){
        save_perfil();
    });

    $('body').on('click', '#perfil_del', function(){
        delete_perfil($(this).closest('tr').attr('perfil-id'));
    });

    $('body').on('click', '#perfil_roles', function(){
        $('#txt_perfil_id').text(parseInt($(this).closest('tr').attr('perfil-id')));
        get_perfil_rol();
        $('#modal_perfil_roles').modal();
    });

    $('body').on('click', '#perfil_edit', function(){
        $('#inp_desc').val($(this).closest('tr').find('.perfil_desc').text());
        $('#cbx_perfil').prop('checked', parseInt($(this).closest('tr').find('.perfil_activo').attr('data-activo')));
        $('#txt_id_perfil').text($(this).closest('tr').attr('perfil-id'));
        $('#modal_perfiles').modal();
    });

    $('#modal_perfiles').on('hidden.bs.modal', function () {
        //Limpiar id del modal
        $('#txt_id_perfil').text('');
        $('#inp_desc').val('');
        $('#cbx_perfil').prop('checked', true);
    })
    $(document).ready(function(){
        get_perfiles();
        get_roles();
    });
    function get_perfiles(){
        const obj = {
            opc : 'get_perfiles'
        }

        send_request('logic/perfiles_logic.php', obj, '#tbl_perfiles');
    }

    function save_perfil_rol(){
        const rol_id = 0;
        const perfil_id = 0;
        const obj = {
            opc : 'insert_perfil_rol',
            rol_id : rol_id,
            perfil_id : perfil_id
        }
        send_request('logic/perfiles_logic.php', obj, '#msgRolesPerfil');
    }

    function get_perfil_rol(){
        const perfil_id = $('#txt_perfil_id').text();

        const obj = {
            opc : 'get_perfil_rol',
            id : perfil_id
        }
        send_request('logic/perfiles_logic.php', obj, '#tbl_perfil_roles');
    }

    function save_perfil(){
        const perfil_id = $('#txt_id_perfil').text();
        const descripcion = $('#inp_desc').val();
        const activo = $('#cbx_perfil').prop('checked') ? 1 : 0;

        if(descripcion == ''){
            return;
        }

        const obj = {
            opc : 'insert_perfil',
            id : perfil_id,
            descripcion : descripcion,
            activo : activo
        }
        send_request('logic/perfiles_logic.php', obj, '#msgPerfilesModal', function () {
            $('#modal_perfiles').modal('hide');
        });
    }

    function delete_perfil(perfil_id){
        const obj = {
            opc : 'delete_perfil',
            id : perfil_id
        }
        send_request('logic/perfiles_logic.php', obj, '#msgPerfiles');
    }

    function get_roles() {
        const obj = {
            opc : 'get_roles'
        }
        send_request('logic/perfiles_logic.php', obj, '#sel_roles');
    }

</script>

<?php
utilities::load_template_footer($cb);
?>
