<?php

/* Se recibe un string de emails separados por , */
$emails = isset($_POST['email']) ? $_POST['email'] : '';
/* Asi mismo el subject, y el body */
$subject = isset($_POST['subject']) ? filter_var($_POST['subject'], FILTER_SANITIZE_STRING, FILTER_NULL_ON_FAILURE) : null;
$body = isset($_POST['body']) ? $_POST['body'] : null;

/* Validar subject */
if($subject == null){
    echo "No se recibió asunto.";
    return;
}
/* Validar cuerpo */
if($body == null){
    echo "No se recibió cuerpo.";
    return;
}
$arreglo_emails = array();
/* Validar emails */
/* 1-Si está vacío */
if($emails == ''){
    echo "No existen emails a enviar.";
    return;
}
/* 2-Emails separados por coma */
if(strpos($emails, ',') == false){
    /* Es un solo email,  sanitizar y agregar al arreglo */
    $emails = sanitizar_correo($emails);
    if($emails != null){
        array_push($arreglo_emails, $emails);
    }

} else {
    /* Son varios separados por coma, agregar al arreglo */
    $emails = dividir_correo($emails); /* Divididos */
    foreach($emails as $email){
        /* Sanar */
        $email = sanitizar_correo($email);
        if($email != null){
            /* Si no está null, es porque es valido */
            array_push($arreglo_emails, $email);
        }
    }
}
/* Inicializacion de headers */
// Para enviar un correo HTML, debe establecerse la cabecera Content-type
$headers  = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=utf8' . "\r\n";
$headers .= 'From: HCM <admin@hcm.gsuiterd.com>' . "\r\n";



/* Validar que existan emails */
if(count($arreglo_emails) > 0){
    foreach($arreglo_emails as $email){
        mail($email, $subject, $body, $headers);
    }
    echo "Correo enviado de forma correcta.";
} else {
    /* No existen, lanzar error */
    echo "Error: No existen emails para enviar mensaje.";
}



function sanitizar_correo($email){
    return filter_var($email, FILTER_SANITIZE_EMAIL, FILTER_NULL_ON_FAILURE);
}

function dividir_correo($emails){
    return explode(',', $emails);
}