<?php
?>

<div class="modal fade" id="modal_categoria" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popin">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Categorias<!-- TITULO --></h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">

                    <div class="row ">
                        <!-- CONTENIDO -->
                        <span class="span-id" id="categoria_id" hidden></span>
                        <div class="col-12 form-group">
                            <label>Nombre Categoria: </label>
                            <input type="text" class="form-control" name="categoria_nombre" id="categoria_nombre">
                        </div>
                        <div class="col-12 form-group">
                            <label>Orden: </label>
                            <input type="number" class="form-control" id="categoria_orden" name="categoria_orden">
                        </div>
                        <div class="col-12 form-group">
                            <label>Icono: </label>
                            <select class="form-control faFont" id="selIcono"></select>
                        </div>
                        <div class="col-12 form-group">
                            <label>Activa: </label>
                            <input type="checkbox" class="form-control" name="categoria_activa" id="categoria_activa">
                        </div>
                        <div class="col-12 msg-error" id="msg_error_categoria">

                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-alt-primary" id="guardar_categoria">
                    <i class="fa fa-check"></i> Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal_categoria_opc" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popin modal-lg">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Opciones de la Categoria<!-- TITULO --> <span id="desc_categoria_opc"></span></h3>
                    <span hidden id="idCategoria"></span>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="row form-group">
                        <div class="col-12 form-group">
                            <!-- SELECT -->
                            <label>Opción:</label>
                            <select class="form-control" id="sel_opc"></select>
                        </div>
                        <div class="col-12 form-group">
                            <label>Orden: </label>
                            <input type="number" class="form-control" id="inp_orden">
                        </div>
                        <div class="col-12 form-group text-center">
                            <button type="button" class="btn btn-primary" id="btn_agregar_opc">Agregar <i class="fa fa-plus"></i></button>
                        </div>

                        <div class="col-12 form-group">
                            <div id="categoriaMsg"></div>
                        </div>
                    </div>
                    <hr>
                    <div class="row form-group">
                        <!-- CONTENIDO -->
                        <div class="col-12" id="tbl_cat_opciones">

                        </div>
                        <div class="col-12" id="msg_opc_cat"></div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-alt-primary">
                    <i class="fa fa-check"></i> Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        get_select_opciones();
        get_icono();
    });
    function guardar_categoria(callback = ''){
        const categoria_id = $('#categoria_id').text();
        const categoria_nombre = $('[name=categoria_nombre]').val();
        const categoria_orden = $('[name=categoria_orden]').val();
        const categoria_activa = $('[name=categoria_activa]').prop('checked') ? 1 : 0;
        const categoria_icono = $('#selIcono option:selected').val();

        var componente_error = $('#msg_error_categoria').html('');

        if(categoria_nombre == ''){
            componente_error.html(send_danger_msg(''));
            return;
        }
        if(categoria_orden == ''){
            componente_error.html(send_danger_msg(''));
            return;
        }


        const obj = {
            opc : 'save_categoria',
            categoria_id : categoria_id,
            categoria_nombre : categoria_nombre,
            categoria_orden : categoria_orden,
            categoria_activa : categoria_activa,
            categoria_icono : categoria_icono
        }

        send_request('logic/categorias_logic.php', obj, componente_error, callback);
    }

    function abrir_modal_categoria() {
        $('#modal_categoria').modal();
    }

    function edit_categoria(id, nombre, orden, activada, icono){
        limpiar_modal_categoria();
        $('#categoria_id').text(id);
        $('[name=categoria_nombre]').val(nombre);
        $('[name=categoria_orden]').val(orden);
        $('[name=categoria_activa]').prop('checked', activada);
        $('#selIcono').val(icono);
        abrir_modal_categoria();
    }

    function nueva_categoria() {
        limpiar_modal_categoria();
        abrir_modal_categoria();
    }

    function limpiar_modal_categoria(){
        limpiar_componentes($('#modal_categoria input, #modal_opciones .span-id, #modal_categoria select'));
    }

    function get_categoria_opciones(categoria_id = 0){
        if(categoria_id == 0){
            /* Fallback en caso de que sea 0 */
            categoria_id = $('#idCategoria').text();
        }
        const obj = {
            opc : 'get_categoria_opciones',
            categoria_id : categoria_id
        }
        send_request('logic/categorias_logic.php', obj, '#tbl_cat_opciones');
        $('#modal_categoria_opc').modal();
    }

    $('body').on('click','#del_opc_cat', function(){
        const id = $(this).closest('tr').attr('data-id');
        const obj = {
            opc : 'delete_categoria_opcion',
            id : id
        }
        send_request('logic/categorias_logic.php', obj, '#msg_opc_cat');
    });

    $('body').on('click', '#save_opc_cat', function(){
        const id = $(this).closest('tr').attr('data-id');
        const orden = $(this).closest('tr').find('input[type=number]').val();
        saveCategoriaOpcion(0,0,orden, id);
    });

    $('#btn_agregar_opc').click(function(){
        const opcion_id = $('#sel_opc option:selected').val();
        const categoria_id = $('#idCategoria').text();
        const orden = $('#inp_orden').val();
        saveCategoriaOpcion(categoria_id, opcion_id, orden);
    });

    function saveCategoriaOpcion(categoria_id = 0, opcion_id = 0, orden = 0, id = 0){
        if(opcion_id == 0 && id == 0){
            return;
        }

        const obj = {
            opc : 'save_opc_categoria',
            opcion_id : opcion_id,
            categoria_id : categoria_id,
            id : id,
            orden : orden
        }

        send_request('logic/categorias_logic', obj, '#categoriaMsg');
    }

    function get_select_opciones(){
        const obj = {
            opc : 'get_sel_opciones'
        }
        send_request('logic/opciones_logic.php', obj, '#sel_opc');
    }

    function get_icono(){
        var comp = $('#selIcono');
        var obj = {
            opc : 'get_icono'
        }
        send_request('logic/categorias_logic.php', obj, comp);
    }
</script>
