<?php
?>

<div class="modal fade" id="modal_opciones" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-dialog-popin">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title">Opción</h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="row">
                        <span id="opcion_id" hidden class="span-id"></span>
                        <div class="col-12 form-group">
                            <label>Descripción</label>
                            <input type="text" class="form-control" id="opcion_descripcion">
                        </div>
                        <div class="col-12 form-group">
                            <label>Link: </label>
                            <input type="text" class="form-control" id="opcion_link">
                        </div>
                        <div class="col-12 form-group">
                            <label>Activado: </label>
                            <input type="checkbox" class="form-control" id="opcion_activado">
                        </div>
                        <div class="col-12 form-group" id="opcion_msg_error">

                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-alt-primary" id="btn_opcion_guardar">
                    <i class="fa fa-check"></i> Guardar
                </button>
            </div>
        </div>
    </div>
</div>

<script>
    function guardar_opcion(callback = ''){
        const id = $('#opcion_id').text();
        const link = $('#opcion_link').val();
        const descripcion = $('#opcion_descripcion').val();
        const activado = $('#opcion_activado').prop('checked') ? 1 : 0;

        var componente_error = $('#opcion_msg_error').html('');
        //Validaciones
        if(descripcion == ''){
            componente_error.html(send_danger_msg('Error: Debe insertar la descripción.'));
            return;
        }
        if(link == ''){
            componente_error.html(send_danger_msg('Error: Debe insertar un link.'));
            return;
        }

        const obj = {
            opc : 'save_opcion',
            id : id,
            descripcion : descripcion,
            link : link,
            activado : activado
        }
        send_request('logic/opciones_logic.php', obj, componente_error, callback);
    }

    function abrir_modal_opciones() {
        $('#modal_opciones').modal();
    }

    function nueva_opcion(){
        limpiar_modal_opciones();
        abrir_modal_opciones();
    }

    function edit_opcion(id, descripcion, link, activada){
        limpiar_modal_opciones();
        $('#opcion_id').text(id);
        $('#opcion_descripcion').val(descripcion);
        $('#opcion_link').val(link);
        $('#opcion_activado').prop('checked', activada);
        abrir_modal_opciones();
    }

    function limpiar_modal_opciones(){
        limpiar_componentes($('#modal_opciones input, #modal_opciones .span-id'));
    }


</script>