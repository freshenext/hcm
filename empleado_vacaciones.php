<?php
require_once __DIR__ . '/logic/utl.php';

utilities::load_template();

?>

<div class="content">
    <div class="row">
        <div class="col-12">
            <div class="block block-themed">
                <div class="block-header">
                    <h3 class="block-title">
                        Vacaciones
                    </h3>
                </div>
                <div class="block block-content">
                    <!-- FILTROS -->
                    <div class="row form-group">
                        <div class="col-4">
                            <div class="form-material">
                                <label>Fecha de Solicitud Creada:</label>
                                <input type="date" class="form-control flatpicker_class" id="fecha_solicitud_creada" placeholder="Click para ver">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-material">
                                <label>Fecha inicio de vacaciones:</label>
                                <input type="date" class="form-control flatpicker_class" id="fecha_inicio" placeholder="Click para ver">
                            </div>
                        </div>
                        <div class="col-4">
                            <div class="form-material">
                                <label>Fecha fin de vacaciones:</label>
                                <input type="date" class="form-control flatpicker_class" id="fecha_fin" placeholder="Click para ver">
                            </div>
                        </div>

                    </div>
                    <!-- Tabla -->
                    <div class="row form-group">
                        <div class="col-12" id="tbl_emp_vacaciones">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal" id="modalVacaciones" tabindex="-1" role="dialog" aria-labelledby="modal-normal" aria-modal="true" >
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="block block-themed block-transparent mb-0">
                <div class="block-header bg-primary-dark">
                    <h3 class="block-title" id="title_estado_vacaciones"></h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-dismiss="modal" aria-label="Close">
                            <i class="si si-close"></i>
                        </button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="modal-body">
                            <span id="estado_vacaciones" hidden></span>
                            <span id="vacaciones_id" hidden></span>
                            <div class="col-12">
                                <div><span style="font-weight: bold;">Empleado: </span><span id="empleado_nombre"></span></div>
                                <div><span style="font-weight: bold;">Fecha Inicio: </span><span id="fecha_inicio_lbl"></span></div>
                                <div><span style="font-weight: bold;">Empleado:</span><span id="fecha_fin_lbl"></span></div>
                            </div>
                            <div class="col-12">
                                <label>Año vacaciones: </label>
                                <input type="number" class="form-control" id="ano_vacaciones">
                            </div>
                            <div class="col-12">
                                <label style="font-weight: bold;">Razón: </label>
                                <textarea class="form-control" rows="7" id="txt_razon"></textarea>
                            </div>
                            <div class="col-12" style="margin-top: 1em; margin-bottom: 1em;" id="msgVacacionesEstado">

                            </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">

                <button type="button" class="btn btn-alt-success" style="font-weight: bold;" id="btn_guardar_estado_vacaciones">
                    <i class="fa fa-check"></i> Guardar
                </button>
                <button type="button" class="btn btn-alt-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>


<script>

    function get_emp_vacaciones(){
        var obj = {
            opc : 'get_emp_vacaciones'
        }
        send_request('logic/vacaciones_logic.php', obj, '#tbl_emp_vacaciones');
    }

    $(document).ready(function () {
        get_emp_vacaciones();
        $('.flatpicker_class').flatpickr();

        $('body').on('click','#btn_aprobar_vac',function(){
            //Aprobar vacaciones
            clean_modal();
            $('#title_estado_vacaciones').text('Aprobar vacaciones');
            $('#estado_vacaciones').text('1');
            set_empleado_info($(this).closest('tr'));
            $('#modalVacaciones').modal();
        });
        $('body').on('click','#btn_rechazar_vac',function(){
            //Rechazar vacaciones
            clean_modal();
            $('#title_estado_vacaciones').text('Rechazar vacaciones');
            $('#estado_vacaciones').text('2');
            set_empleado_info($(this).closest('tr'));
            $('#modalVacaciones').modal();
        });

        $('#btn_guardar_estado_vacaciones').click(function(){
            const vacacion_id = parseInt($('#vacaciones_id').text());
            const estado_vacacion = parseInt($('#estado_vacaciones').text());
            const razon = $('#txt_razon').val();
            const ano_vacaciones = $('#ano_vacaciones').val();
            const obj = {
                opc : 'guardar_vacaciones',
                id : vacacion_id,
                estado : estado_vacacion,
                razon : razon,
                ano_vacaciones : ano_vacaciones
            }

            send_request('logic/vacaciones_logic.php',obj, '#msgVacacionesEstado',function () {
                //Al guardar, va a buscar las vacaciones activas
                get_emp_vacaciones();
            });
        });

        function set_empleado_info(tr){
            $('#empleado_nombre').text(tr.find('.td_nombre').text());
            $('#fecha_inicio_lbl').text(tr.find('.td_fecha_inicio').text());
            $('#fecha_fin_lbl').text(tr.find('.td_fecha_fin').text());
            $('#vacaciones_id').text(tr.attr('data-id'));
        }

        function clean_modal(){
            $('#txt_razon').val('');
            $('#empleado_nombre, #fecha_inicio_lbl, #fecha_fin_lbl, #estado_vacaciones').text('');
        }
    });
</script>