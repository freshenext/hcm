<?php
require_once __DIR__ . '/logic/utl.php';

$cb = utilities::load_template();
?>
<?php include_once __DIR__ . '/modal/modal_categorias.php'; ?>
<div class="content">


    <!-- CONTENIDO -->
    <div class="row">
        <div class="col-12">
            <div class="block block-themed">
                <div class="block-header">
                    <h3 class="block-title">Categorias</h3>
                </div>
                <div class="block-content">
                    <!-- Filtros de busqueda -->
                    <div class="row">
                        <div class="col-12 text-center">
                            <button type="button" class="btn btn-primary" id="categoria_add">Añadir categoria</button>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12" id="msgCategorias">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12" id="tbl_categorias">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $('#categoria_add').click(function () {
            nueva_categoria();
        });

        $('#guardar_categoria').click(function () {
            guardar_categoria(function () {
                $('#modal_categoria').modal('hide');
                load_categorias();
            });
        });

        function load_categorias() {
            var obj = {
                opc : 'get_categorias'
            }
            send_request('logic/categorias_logic.php', obj, '#tbl_categorias');
        }

        load_categorias();

        $('body').on('click', '[name=edit_categoria]', function(){
            var tr = $(this).closest('tr');
            var id = tr.attr('data-id');
            var desc = tr.find('#categoria_desc').text();
            var orden = tr.find('#categoria_orden').text();
            var activo = tr.attr('data-activo') == 1 ? true : false;
            var icono = tr.attr('data-icono') == '' ? 0 : tr.attr('data-icono');
            edit_categoria(id, desc, orden, activo, icono);
        });

        $('body').on('click', '[name=agregar_opc_cat]', function(){
            const categoria_id = $(this).closest('tr').attr('data-id');
            const descripcion = $(this).closest('tr').find('#categoria_desc').text();
            $('#idCategoria').text(categoria_id);
            $('#desc_categoria_opc').text(descripcion);
            get_categoria_opciones(categoria_id);
        });

        $('body').on('click', '[name=del_categoria]', function () {
            const id = $(this).closest('tr').attr('data-id');
            const obj = {
                opc : 'delete_categoria',
                id : id
            }
            send_request('logic/categorias_logic.php', obj, '#msgCategorias');
        });
    </script>
</div>


</div>

<?php
utilities::load_template_footer($cb);
?>
