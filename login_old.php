<?php
//Validar que $_SESSION no exista.
if(isset($_SESSION)){
    header('Location: /index.php');
}

require 'inc/_global/config.php';
require 'inc/_global/views/head_start.php';
require 'inc/_global/views/head_end.php';
require 'inc/_global/views/page_start.php';

?>
<script src="/assets/js/core/jquery.min.js"></script>
<script src="/logic/utl.js"></script>
<style>
    #main-container {
        background: linear-gradient(-180deg,#000000,#627cff);

        height: 100vh;
    }
    .background_color {
        background-color: whitesmoke;
        border-radius: 2em;
    }
</style>
<div class="content">
    <div class="row background_color animated fadeInDownBig">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <div class="row">
                        <div class="col-12">
                            <div class="text-center"><img src="/images/hcm.png" style="padding: 2em;"></div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-material floating">
                                <input type="text" class="form-control" id="username" name="username">
                                <label for="username">Usuario</label>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-material floating">
                                <input type="password" class="form-control" id="pass" name="pass">
                                <label for="pass">Contraseña</label>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 1em; margin-bottom: 1em;">
                        <div class="col-12">
                            <a href="/">¿Ha olvidado su contraseña?</a>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 1em; margin-bottom: 1em;">
                        <div class="col-12">
                            <div class="text-center">
                                <button class="btn btn-hero btn-success btn-rounded" data-toggle="click-ripple" id="iniciar_sesion">Iniciar Sesión</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>




<?php require 'inc/_global/views/page_end.php'; ?>
<?php require 'inc/_global/views/footer_start.php'; ?>
<?php require 'inc/_global/views/footer_end.php'; ?>

<script>
    $('#iniciar_sesion').click(function () {
        send_request('logic/model_login.php',{}, '');
    });

</script>
